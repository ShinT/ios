//
//  Audio.h
//
//  Created by Shin Tan on 5/5/13.
//  Copyright (c) 2013 PimpFit.com All rights reserved.
//

/**
 Desc:
 * Audio class for sound based media playback
 */
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

/* TODO:
 */

/////////////////////////////////////////////////
#pragma mark - protocol delegate
@class Audio;
@protocol AudioDelegate <NSObject>

@optional

-(void)audio:(Audio*)audio willStartPlayingTrack:(NSString*)track withId:(id)theId;

-(void)audio:(Audio*)audio didFinishPlayingTrack:(NSString*)track withId:(id)theId withCompletedStatus:(BOOL)completed;
@end

/////////////////////////////////////////////////
#pragma mark - interface
@interface Audio : NSObject  
<AVAudioPlayerDelegate> {}

////////////////////////////////////////////////
#pragma mark - public enum

////////////////////////////////////////////////
#pragma mark - delegate property
@property(weak,nonatomic) id delegate;
////////////////////////////////////////////////////
#pragma mark - class property

/**retain || strong. Else, prob for 5.1 sdk arc - ptr out of scope on exit cycle. */
@property(strong,nonatomic) AVAudioPlayer *pAvaPlayer;

/**
 id class associated w/ track.
 Optional: nil.
 In use: delegates.
 */
@property(copy,nonatomic) id pTrackId;

@property(assign, nonatomic) float pDefaultVolume;
////////////////////////////////////////////////////
#pragma mark - property of type enum:

////////////////////////////////////////////////////
#pragma mark - public static getter
+(BOOL) sgShouldPlayAudio;
////////////////////////////////////////////////////
#pragma makr - public static setter
+(void) ssShouldPlayAudio:(BOOL)value;
///////////////////////////////////////////////////
#pragma mark - custom constructors

/**
 * bug fix.
 * preload avaPlayer obj at start time due to a few sec lag in 1st time instantiating of audio obj.
 Optional: audio id - nil.
 */
-(id) initWithoutLagWithAudioFileNamed:(NSString*)fileNamed withId:(id)theId;

////////////////////////////////////////////////////
#pragma mark - public method declarations

-(void) fsOverrideVolume:(float)volume;

/**
 id class associated w/ track.
 Optional: nil.
 In use: delegates.
 fileNamed x.m4a
 */
-(void) fsPlayAudioFileNamed:(NSString*)fileNamed withId:(id)theId atVolume:(float)volume;

/**
 * stop any active playing audio
 */
-(void) fsStopActiveTrack;

/**
 *additional tracks queue - played after initial one completes
 */
-(void) fsQueueAdditionalTrackWithFileNamed:(NSString*)fileNamed withId:(id)theId;

/**
 id class associated w/ track.
 Optional: nil.
 In use: delegates.
 */
//-(void) setPlayAudioWithId:(id)theId fromFileNamePart:(NSString*)fileNamePart ofType: (NSString*)type atVolume:(float)volume;
//empty - initWithoutLag workaround
//-(void) setReady;
@end
