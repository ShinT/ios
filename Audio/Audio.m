//
//  Audio.m
//
//  Created by Shin Tan on 5/5/13.
//  Copyright (c) 2013 PimpFit.com All rights reserved.
//
/**
- Testing: Class switch const. Off: No audio play.
 */
#import "Audio.h"

/*
 TODO:
 */

@implementation Audio {
    NSMutableArray *_gNsmaAdditionalTracksQueue;
}

////////////////////////////////////////////////////
#pragma mark - const
/**0.0 - 1.0 avaudio gain*/
static const float sConstDefaultAudioVolume = 1.0;

//Testing: Class switch const. Off: No audio play. Avoid excessive ptrs during recompile.

static NSString *const lkFileNamed = @"lkFileNamed";
static NSString *const lkId = @"lkId";
///////////////////////////////////////////////////
#pragma mark - static var
/** t: prod, f: testing */
static BOOL  sConstShouldPlayAudio = YES;
////////////////////////////////////////////////////
#pragma mark - delegate property synthesis

////////////////////////////////////////////////////
#pragma mark - class property synthesis

////////////////////////////////////////////////////
#pragma mark - private getter
-(NSMutableArray*) gNsmaAdditionalTracksQueue {
    if (_gNsmaAdditionalTracksQueue == nil) {
        _gNsmaAdditionalTracksQueue = [[NSMutableArray alloc]init];
    }
    return _gNsmaAdditionalTracksQueue;
}
////////////////////////////////////////////////////
#pragma mark - public static getter
+(BOOL) sgShouldPlayAudio {
    return sConstShouldPlayAudio;
}
////////////////////////////////////////////////////
#pragma makr - public static setter
+(void) ssShouldPlayAudio:(BOOL)value {
    sConstShouldPlayAudio = value;
}
///////////////////////////////////////////////////
#pragma mark - constructor

/*
 * convenience construct not possible - 1st time lag issue.
 * caller needs to init, then hold on to the ptr in a getter for each replay. Else, each construct re-creates the lag issue.
 */

/**
 * to preload avaPlayer obj at start time, play audio at 0 vol due to a few sec lag in 1st time instantiating of audio obj
 */
-(id) initWithoutLagWithAudioFileNamed:(NSString*)theFileNamed withId:(id)theId {
    
    //super init
    if (self = [super init]) {
        // play a short audio @ 0 vol to get around lag bug
        if (!sConstShouldPlayAudio) {return self;};
        
        //play at no volume to init avaPlayer only
        [self fsPlayAudioFileNamed:theFileNamed withId:theId atVolume:0.0];
        
        //now, init to default volume
        self.pDefaultVolume = sConstDefaultAudioVolume;
       
    }
        
    return self;   
}

/* not in use - 1st time lag issue
 -(id) init {
 if (self = [super init]) {
 //init
 }
 //init default volume, unless overriden in play method
 defaultVolume = constDefaultAudioVolume;
 return self;
 }
 */

///////////////////////////////////////////////////
#pragma mark - destructor
/* no longer required - per ARC
-(void) dealloc {
    //[super dealloc];
    [self setAvaPlayer:nil];
}
 */

////////////////////////////////////////////////////
#pragma mark - public method implementation

/* Optional: caller to temporarily override defaultVolume property setter.
 * to make sure any value >0, is set to const default vol instead.
 * overmax vol seems to affect gain
 */
 -(void) fsOverrideVolume:(float)volume; {
    if (volume > 0) {
        self.pDefaultVolume = sConstDefaultAudioVolume;
    }
    else {
        self.pDefaultVolume = volume;
    }
}
////////////////////////////////////////////////

/**
 * play audio w/ avaplayer obj from full filename
  bug: pAvaPlayer sometimes nil w/ certain files?
 reason: when resetting file names as Constants. 
 solution: after creating constants, delete affected files, then re-add in xcode
 note: media file needs to be in root directory.

 */
-(void) fsPlayAudioFileNamed:(NSString*)fileNamed withId:(id)theId atVolume:(float)volume;
  {
    
    if (!sConstShouldPlayAudio) {
        return;
    };
    
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle]resourcePath], fileNamed];
    
    NSURL *urlPath = [NSURL fileURLWithPath:filePath];
      
    self.pAvaPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlPath error:nil];
      
    //avaPlayer.numberOfLoops = x;
    
    self.pAvaPlayer.volume = volume;
    
    if (!theId) {theId = @"";} //else, comparative craps out
    self.pTrackId  = theId;
      
    [self.pAvaPlayer setDelegate:self];
      
      if (self.pAvaPlayer) {
          [[self delegate]audio:self willStartPlayingTrack:fileNamed withId:theId];
      }
      else {
          [[self delegate]audio:self willStartPlayingTrack:@"" withId:theId];
      }
      
    /*prepare to play - preloads buffer + acquires hardware to play.
      note: prepareToPlay - cause track skipping at times when scheduling multiple tracks? Need more testing
     */
    //[self.pAvaPlayer prepareToPlay];
     
    [self.pAvaPlayer play];
    
    //testing
    //dLog(@"played: %@",fileName);
}

///////////////////////////////////////////////////
#pragma mark - stop active Audio

/**
 * stop any active playing audio
 */
-(void) fsStopActiveTrack {
    if (self.pAvaPlayer) {
        switch([self.pAvaPlayer isPlaying]) {
            case NO: {break;}
            case YES: {
                [self.pAvaPlayer stop];
                break;
            }
                
        }//s
    }
}
////////////////////////////////////////////////////
#pragma mark - additional tracks

/* queue additional tracks to be played after the initial one
 * 
 */
-(void)fsQueueAdditionalTrackWithFileNamed:(NSString*)fileNamed withId:(id)theId{
    if (!theId) {theId = @"";} //otherwise, comparative craps out
   
    NSDictionary *nsdAdditionalTrack = @{lkFileNamed:fileNamed, lkId:theId};
    
    [[self gNsmaAdditionalTracksQueue]addObject:nsdAdditionalTrack];
    
    //[[self gNsmaAdditionalTracksQueue]addObject:fileNamed];
}
////////////////////////////////////////////////////

/**
 * play audio w/ file name parts
 * not in use.
 */
/*
 -(void) setPlayAudioFromFileNamePart:(NSString*) fileNamePart ofType:(NSString*) type atVolume:(float) volume {
 
 //global check
 if (!scShouldPlayAudio) {return;};
 
 //note: media file needs to be in root dir
 
 //set filePath using 2 separate fileName parts (name+ ext) and specify bundle(dir)
 NSString *filePath = [[NSBundle mainBundle] pathForResource:fileNamePart ofType:type];
 
 //set filepath to urlPath
 NSURL *urlPath = [NSURL fileURLWithPath:filePath];
 
 //init av player obj
 pAvaPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlPath error:nil];
 
 //indicate loops
 //avaPlayer.numberOfLoops = x;
 
 //volume
 pAvaPlayer.volume = volume;
 
 //delegate
 if (pAvaPlayer) {
 [[self delegate]audio:self willStartPlayingTrack:[NSString stringWithFormat:@"%@.%@",fileNamePart,type]];
 }
 else {
 [[self delegate]audio:self willStartPlayingTrack:@""];
 }
 
 //play audio
 [pAvaPlayer play];
 
 }
 */
///////////////////////////////////////////////
/* empty  - initWithoutLag workaround
 * this just access the lazy loader and alloc audio obj the 1st time.
 Not in use.
 */
//-(void) setReady {};

///////////////////////////////////////////////////////
#pragma mark - private method implementation

/**
 dequeue additional track after done.
*/
 -(void) fsDequeueAdditionalTrack {
    ([[self gNsmaAdditionalTracksQueue]count] >0) ? [[self gNsmaAdditionalTracksQueue]removeObjectAtIndex:0] :nil;
}
//////////////////////////////////////////////

/** check additional tracks queue.
 */
-(BOOL) fgIsAdditionalTracksQueueEmpty {
    return ([[self gNsmaAdditionalTracksQueue]count]==0) ? YES: NO;
}
//////////////////////////////////////////////

/** pick up next track in queue.
 * dequeue it after.
 */
 -(NSDictionary*) fgNextAdditionalTrackInQueue {
  NSDictionary *o = [[self gNsmaAdditionalTracksQueue]objectAtIndex:0];
   
     //clean up
    [self fsDequeueAdditionalTrack];
    return o;
}
///////////////////////////////////////////////////////
#pragma mark - delegate callback listeners

#pragma mark - AVAudio delegates

-(void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
   //notify caller work done
    
    if (flag) {
        //done playing -> delegate
        [[self delegate]audio:(Audio*)self didFinishPlayingTrack:player.url.lastPathComponent withId:self.pTrackId withCompletedStatus:flag];
        //dLog(@"audio-finished playing: %@",player.url.lastPathComponent);
    }
   
    //check additional tracks queue. Do task necessary
    if (![self fgIsAdditionalTracksQueueEmpty]) {
        NSDictionary *nsdTrack = [self fgNextAdditionalTrackInQueue];
    
    //[PimpFitDataModel setTestLogForClass:@"Audio" withMethod:@"didFinishPlaying" withKey:@"nsdTrack" withValue:[NSString stringWithFormat:@"%@",nsdTrack]];
        
        [self fsPlayAudioFileNamed:[nsdTrack objectForKey:lkFileNamed ]withId:[nsdTrack objectForKey:lkId]atVolume:self.pDefaultVolume];
    }//i
    
    //(![self fgIsAdditionalTracksQueueEmpty]) ? [self fsPlayAudioFromFileNamed:[self fgNextAdditionalTrackInQueue] atVolume:self.pDefaultVolume]: nil;
}

/* xcode < 4.6 - bug: it raised some strange error: Error loading /System/Library/Extensions/AudioIPCDriver.kext/Contents/Resources/AudioIPCPlugIn.bundle/Contents/MacOS/AudioIPCPlugIn:  dlopen(/System/Library/Extensions/AudioIPCDriver.kext/Contents/Resources/AudioIPCPlugIn.bundle/Contents/MacOS/AudioIPCPlugIn, 262): Symbol not found: ___CFObjCIsCollectable

 xcode >= 4.6 still shows strange error in output window. But, now delegate seems to be ok. Ok to use for now.
*/

///////////////////////////////////////////////////

 
@end
