//
//  PimpFitJSON.m
//
//  Created by Shin Tan on 12/22/13.
//  Copyright (c) 2013 PimpFit.com. All rights reserved.
//

/**
 - TODO:
 */

#import "PimpFitJSON.h"
#import "PimpFitUIModel.h"
////////////////////////////////////////////////////
#pragma mark - implementation
@implementation PimpFitJSON {
    
    NSDictionary *_gNsdJSONData;
}

////////////////////////////////////////////////////
#pragma mark - public getters

-(NSDictionary*)gNsdJSONData {
    return _gNsdJSONData;
}
///////////////////////////////////////////////////
#pragma mark - public setters

////////////////////////////////////////////////////
#pragma mark - delegate property synthesis

////////////////////////////////////////////////////
#pragma mark - class property synthesis

///////////////////////////////////////////////////
#pragma mark - private getters

///////////////////////////////////////////////////
#pragma mark - private setters

////////////////////////////////////////////////////
#pragma mark - private static const

//TODO:- Refer readme
static NSString *const scJSONTestURLNamed = @"";

static NSString *const scTestConnectionURLNamed = @"";
///////////////////////////////////////////////////
#pragma mark - private static vars

////////////////////////////////////////////////////
#pragma mark - public static getters
+(NSString*)sgetJSONTestURLNamed {
    return scJSONTestURLNamed;
}

/////////////////////////////////////
+(NSString*)sgetTestConnectionURLNamed {
    return scTestConnectionURLNamed;
}
///////////////////////////////////////////////////
#pragma mark - public static setters

///////////////////////////////////////////////////
#pragma mark - constructors

/*
+(id) requestJSONDataWithURLNamed:(NSString*)theURLNamed completion:(void(^)( PimpFitJSONResult result))completion withObject:(PimpFitJSON*)theObject {
    return [[PimpFitJSON alloc]initRequestWithURLNamed:theURLNamed completion:completion ];
    
}
*/
//////////////////////////////////////////////////

-(id) init {
    if (self = [super init]) {
        
    }
    return self;
}

//////////////////////////////////////////
#pragma mark - destructor

////////////////////////////////////////////////////
#pragma mark - class/static method implementation

////////////////////////////////////////////////////
#pragma mark - public method implementation

/**
 * JSON data request.
 * Caller: handle completion block.
 * optional: check internet connection.
 */
-(void) requestJSONDataFromURLWithString:(NSString*)theURLNamed completion:(void(^)(PimpFitJSONResult result))completion {
    
    _gNsdJSONData = [self getJSONSerializedData:[self getRawDataWithRequestURLNamed:theURLNamed]];
   
    if (_gNsdJSONData && (!self.pErrorResult)) {
        completion(PimpFitJSONResultSuccess);
    }
    else {
        completion(PimpFitJSONResultFail);
    }

    
    
}
////////////////////////////////////////////////////

/**
 * JSON data request - async.
 * Caller: handle completion block.
 * optional: check internet connection.
 */
-(void) requestJSONDataAsyncFromURLWithString:(NSString*)theURLNamed completion:(void(^)(PimpFitJSONResult result))completion {
    
   //bg
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0),^{
        
        //get json
        _gNsdJSONData = [self getJSONSerializedData:[self getRawDataWithRequestURLNamed:theURLNamed]];
        
        //long thread simulation
        switch(self.pIsTesting) {
            case NO: {break;}
            case YES: {
                [PimpFitDataModel setTestLogForClass:@"PimpFitJSON" withMethod:@"requestJSONDataAsyncFromURLWithString" withKey:@"Async Thread" withValue:[NSString stringWithFormat:@"%@",[NSThread currentThread]]];
                //dLog(@"PimpFitJSON - requestJSONDataAsyncFromURLWithString - async thread: %@", [NSThread currentThread]);
                sleep(1.5);
                break;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(),^{
           // dLog(@"async thread: %@",[NSThread currentThread]);
            
            if (_gNsdJSONData && (!self.pErrorResult)) {
                completion(PimpFitJSONResultSuccess);
            }
            else {
                completion(PimpFitJSONResultFail);
            }
            
        });//main b
        
    });//background block
    
}

////////////////////////////////////////////////////
#pragma mark - private method implementation

/**
 * raw data with the request url
 */
-(NSData*) getRawDataWithRequestURLNamed:(NSString*)theURLNamed {

    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:theURLNamed]];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *dat =  [NSURLConnection sendSynchronousRequest:request returningResponse: &response error :&error];
    
    self.pResponseHttpStatusCode = ((NSHTTPURLResponse*)response).statusCode;
    
       [PimpFitDataModel setTestLogForClass:@"PimpFitJSON" withMethod:@"getRawDataWithRequestURLNamed" withKey:@"pResponseHttpStatusCode" withValue:[@(self.pResponseHttpStatusCode)stringValue]];
    
    if (!dat || error) {
        self.pErrorResult = error;
       
        [PimpFitDataModel setTestLogForClass:@"PimpFitJSON" withMethod:@"getRawDataWithRequestURLNamed" withKey:@"pErrorResult" withValue:[NSString stringWithFormat:@"%@",error.debugDescription]];
        
        //[NSString stringWithFormat:@"%i",self.pErrorHttpStatusCode]
        
        //dLog(@"PimpFitJSON - getRawDataWithRequestURLNamed - error result: %@",error.description);
        return nil;
    }
    
  return dat;
}

////////////////////////////////////////////////////////////

/**
 * serialized raw data as JSON
 */
-(NSDictionary*) getJSONSerializedData:(NSData*)theData {
    
    if (!theData) {
        return nil;
    }
    
    NSError *error;
    _gNsdJSONData = [NSJSONSerialization JSONObjectWithData:theData options:kNilOptions error:&error];
    
    if (error || _gNsdJSONData == nil) {
        self.pErrorResult = error;
       
         [PimpFitDataModel setTestLogForClass:@"PimpFitJSON" withMethod:@"getJSONSerializedData" withKey:@"pErrorResult" withValue:[NSString stringWithFormat:@"%@",error.debugDescription]];
        // dLog(@"PimpFitJSON - getJSONSerializedData - error result: %@",error.description);
        return nil;
    }
    
    //dLog(@"serialized json: %@",_nsdJSONData);
    return _gNsdJSONData;
}


////////////////////////////////////////////////////
#pragma mark - delegate callbacks
///////////////////////////////////////////////////
#pragma mark - test
-(void) setTestJSONDataLog {
    NSArray *arrLoans = [self.gNsdJSONData objectForKey:@"loans"];
    //dLog(@"list of loans: %@", arrLoans);
    
    //loan 1
    NSDictionary *dictLoan = [arrLoans objectAtIndex:0];
    
    //loan amt 1
    NSNumber *loanAmt = [dictLoan objectForKey:@"loan_amount"];
    
    //country 1
    NSDictionary *dictLocation = [dictLoan objectForKey:@"location"];
    
    NSString *country = [dictLocation objectForKey:@"country"];
    
   // dLog(@"country 1: %@",country);
   // dLog(@"loan amount 1: %@",loanAmt);
   
    //loan 5
    dictLoan = [arrLoans objectAtIndex:5];
    
    //loan amt 5
    loanAmt = [dictLoan objectForKey:@"loan_amount"];
    
    //country 5
    dictLocation = [dictLoan objectForKey:@"location"];
    country = [dictLocation objectForKey:@"country"];
    
   // dLog(@"country 5: %@",country);
   // dLog(@"loan amount 5: %@",loanAmt);
}
///////////////////////////////////////////////////////////
@end
