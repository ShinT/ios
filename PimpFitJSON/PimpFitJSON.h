//
//  PimpFitJSON.h
//
//  Created by Shin Tan on 12/22/13.
//  Copyright (c) 2013 PimpFit.com. All rights reserved.
//
/**
 * Desc:
   Lighter alternative to AFNetworking.
   Async support via GCD.
 */

/**
 - TODO:
 */

#import <Foundation/Foundation.h>

////////////////////////////////////////////////
#pragma mark - public enum

enum PimpFitJSONResult {
    PimpFitJSONResultFail,
    PimpFitJSONResultSuccess
};

////////////////////////////////////////////////////
#pragma mark - typedef
typedef enum PimpFitJSONResult PimpFitJSONResult;
////////////////////////////////////////////////////
#pragma mark - public const

////////////////////////////////////////////////////
#pragma mark - public getter

/////////////////////////////////////////////////
#pragma mark - protocol delegate

/////////////////////////////////////////////////
#pragma mark - interface

@interface PimpFitJSON : NSObject

////////////////////////////////////////////////
#pragma mark - delegate property


////////////////////////////////////////////////////
#pragma mark - class property

//in
@property(copy,nonatomic) NSString *pRequestURLNamed;
@property(assign,nonatomic) BOOL pIsTesting;

//out
@property(copy,nonatomic) NSError *pErrorResult;
@property(assign,nonatomic) NSInteger pResponseHttpStatusCode;

////////////////////////////////////////////////////
#pragma mark - property of type enum:

////////////////////////////////////////////////////
#pragma mark - property of typedef block

////////////////////////////////////////////////////
#pragma mark - public const

////////////////////////////////////////////////////
#pragma mark - public getters

-(NSDictionary*)gNsdJSONData;

///////////////////////////////////////////////////
#pragma mark - public setters

////////////////////////////////////////////////////
#pragma mark - public static getters

+(NSString*)sgetJSONTestURLNamed;
+(NSString*)sgetTestConnectionURLNamed;

////////////////////////////////////////////////////
#pragma mark - public static setters

/////////////////////////////////////////////////////////
#pragma mark - constructors

///////////////////////////////////////
#pragma mark - class/static method declarations

////////////////////////////////////////////////////
#pragma mark - public method declarations

/**
 * JSON data request.
 * Caller: handle completion block.
 * optional: check internet connection.
 */
-(void) requestJSONDataFromURLWithString:(NSString*)theURLNamed completion:(void (^)(PimpFitJSONResult result))completion;

////////////////////////////////////////////////////

/**
 * JSON data request - async.
 * Caller: handle completion block.
 * optional: check internet connection.
 */
-(void) requestJSONDataAsyncFromURLWithString:(NSString*)theURLNamed completion:(void(^)(PimpFitJSONResult result))completion;
//////////////////////////////////////
#pragma mark - test

-(void) setTestJSONDataLog;

@end
