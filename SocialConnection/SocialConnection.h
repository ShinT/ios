//
//  SocialConnection.h
//  SocialConnection
//
//  Created by Shin Tan on 7/16/13.
//  Copyright (c) 2013 Shin Tan. All rights reserved.
//

/**
 Desc:
 * Social connection options version 1. For IOS <= 6 backwards compat. For IOS > 6, use UIActivityController
 */

/**
 - TODO:
 - Twitter.framework - TWTweetComposeViewController ios >=5
 */

#import <Foundation/Foundation.h>

#import <MessageUI/MessageUI.h>

//#import <Twitter/Twitter.h>

#import "SocialFanVC.h"

//SocialConnectionDetail - required properties for all components
#import "SocialConnectionDetail.h"

//VC2 - setShowRealTimeMessage - limited cmd options support
#import "PimpFitTriggeredTask.h"
////////////////////////////////////////////////
#pragma mark - public enum

enum {
    enumSocialConnectionTypeSMS = 0,
    enumSocialConnectionTypeEmail = 1,
    enumSocialConnectionTypeTwitter = 2,
    enumSocialConnectionTypeFacebook = 3,
    enumSocialConnectionTypeNone = 4
    
}; typedef NSInteger SocialConnectionType;

////////////////////////////////////////////
/** internal socialConnectionTypeStatus. handles various twitter, facebook sdk processing states. Not associated w/ actionsheet options */
enum{
    enumSocialConnectionTypeStatusNone = 0,
    enumSocialConnectionTypeStatusTwitterTWTweetSDKCancelled = 1,
    enumSocialConnectionTypeStatusTwitterTWTweetSDKDone = 2,
    enumSocialConnectionTypeStatusTwitterFallbackWebInterface = 3,
    enumSocialConnectionTypeStatusFacebookCancelled =4,
    enumSocialConnectionTypeStatusFacebookDone = 5,
    enumSocialConnectionTypeStatusFacebookFallbackWebInterface = 6,
    enumSocialConnectionTypeStatusFacebookNoDeviceSetup = 7,
    enumSocialConnectionTypeStatusSMSNoDeviceSetup = 8
    
}; typedef NSInteger SocialConnectionTypeStatus;
///////////////////////////////////////////////////

/**share type */
/*
enum {
    enumShareTypeAppRecommendation = 1,
    enumShareTypeStatusUpdate = 2,
    enumShareTypeCommunication = 3 //communication purposes only
}; typedef NSInteger ShareType;
*/
//now in SocialConnectionDetail
/////////////////////////////////////////////////
#pragma mark - protocol delegate
@class SocialConnection;

@protocol SocialConnectionDelegate <NSObject>

@optional
-(void) socialConnection:(SocialConnection*)socialConnection didCompleteCommunication:(BOOL)completed;
@end

/////////////////////////////////////////////////
#pragma mark - interface
@interface SocialConnection : NSObject
<
UIActionSheetDelegate,
UIAlertViewDelegate,
MFMailComposeViewControllerDelegate,
MFMessageComposeViewControllerDelegate
>
{}

////////////////////////////////////////////////
#pragma mark - delegate property
@property(weak,nonatomic)id delegate;
////////////////////////////////////////////////////
#pragma mark - class property

//in

#pragma mark - social feedback items - caller responsible

//required properties for all components
@property(strong,nonatomic) SocialConnectionDetail *pSocialConnectionDetail;

//socialConnectionFeedbackVCM properties

//feedbackItems master list
@property(strong, nonatomic) NSMutableArray *pNsmaSocialConnectionFeedbackItemsMasterList;

//feedbackItems list to use - to be randomly selected
@property(strong, nonatomic) NSMutableArray *pNsmaSocialConnectionFeedbackItems;

//# of feedback items to be randomly selected. Default 7
//@property(assign, nonatomic) int pSocialConnectionFeedbackNumberOfItems;
/* refer SocialConnectionDetail.pUserSelectableX
@property(copy,nonatomic) NSString *pSocialConnectionFeedbackVCMNavigationItemTitle;
@property(copy,nonatomic)NSString *pSocialConnectionFeedbackVCMBarButtonItemTitle;
@property(copy,nonatomic)NSString *pSocialConnectionFeedbackVCMMainMsg;
*/
//selected feedback item
//@property(copy,nonatomic) NSString
//*pSelectedSocialConnectionFeedbackItem; - replace w/ SocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl


#pragma mark - msg, social network options
//uiactionsheet properties

/**
* uiactionsheet host VC for social options.
* Set this to any parameter VC before showing uiactionsheet.
 * Otherwise, after alertView msg & reshow actionsheet, causes incorrect host VC to be set
*/
@property(strong,nonatomic) UIViewController *pUIActionSheetHostViewController;
//option to keep social options on screen after finished sharing, until user taps away. Default on.
//@property(assign,nonatomic)BOOL pShouldKeepSocialOptionsOpenAfterFinishedSharing;

//recipient - currently used in general messagin
@property(copy,nonatomic) NSArray *pRecipients;

//text msg for uisheet. If "", will use default. 
//@property(copy,nonatomic) NSString *pSocialOptionsMainMsg;
//@property(copy, nonatomic) NSString *pSubject;

//general msg for each social type to be concat w/ other elements
//offers more flexibility for messaging for each social network type
//@property(copy, nonatomic) NSString *pSMSGeneralMessage;
//@property(copy,nonatomic) NSString *pEmailGeneralMessage;
//@property(copy,nonatomic) NSString *pTwitterGeneralMessage;
//@property(copy,nonatomic) NSString *pFacebookGeneralMessage;

//@property(assign, nonatomic) BOOL pIsHTMLBasedEmail;

//share type: whether user's sharing app (recommendation) or status update
//now in SocialConnectionDetail
//@property(assign,nonatomic) ShareType pShareType;

// imessage only supported in ios > 6? ios <6 is pure sms?
//default yes for testing
@property(assign, nonatomic) BOOL pIsIMessage;
/**
 *not in use
 */
//@property(assign,nonatomic) BOOL pTestInSimulatorOnly;

@property(assign,nonatomic) SocialFanType pSocialFanType;

//out

/**
 * fav session. Track to help w/ caller viewAppear suppress/allow process.
 * email, sms & other social delegates trigger caller viewDisappear/Appear.
 * flag on in showSocialNetwork for fav shareType.
 * flag off in alertView dismiss - since sheet is always up after each social thread
 
 */
@property(assign,nonatomic) BOOL pIsSocialConnectionInSession;

/**
 *not in use
 */
//@property(assign,nonatomic) NSInteger *pSelectedSocialConnectionType;
////////////////////////////////////////////////////
#pragma mark - property of type enum:

////////////////////////////////////////////////////
#pragma mark - public getters

///////////////////////////////////////////////////
#pragma mark - public setters
///////////////////////////////////////////////////
#pragma mark - static gets
///////////////////////////////////////////////////
#pragma mark - static gets

////////////////////////////////////////////////////
#pragma mark - constructors

////////////////////////////////////////////////////
#pragma mark - public method declarations

//Share Options

/**
 * main share option.
 * For app review, share app w/ friends.
 * pipe required properties as SocialConnectionDetail.
 * pShareType = enumShareTypeStatusUpdate.
 * NOTE: should subscribe to at least 1 delegate method.
 * delegate self auto-set in SocialConnection.
 * a) Alert Text + Msg.
 * b) Home My Thoughts Feature Request.
 * c) Optional: FeedbackVCM share items. (User Selectable pre-filled text for sharing).
 * d) Configurable plist - after trigger reached, will show only "My Thoughts" option, Then, trigger resets itself.
 */
-(void) setShowShareOptionForViewController:(UIViewController *)theVC withSocialConnection:(SocialConnection*)theSocialConnection withSocialConnectionDetail:(SocialConnectionDetail*)theSocialConnectionDetail;

/*
 withAppName:(NSString*)theAppName withAlertText:(NSString*)theAlertText withScreenMsg:(NSString*)theScreenMsg withVC2ShareMsg:(NSString*)theVC2ShareMsg withVC2ShowLoveMsg:(NSString*)theVC2ShowLoveMsg withVC2ViewBackgroundImageFileNameWithExt:(NSString*)theVC2ViewBackgroundImageFileNameWithExt withVC2WebFallbackOptionUserReviewInstruction:(NSString*)theVC2WebFallbackOptionUserReviewInstruction
 
 */
////////////////////////////////////////////////////

/**
 * main connection point: email, sms, twitter, fb etc.
 * VC in pageviewcontroller needs to be parentViewController.
 */
-(void)setShowSocialNetworkAndCommunicationOptionsForViewController:(UIViewController*)theVC withSocialConnectionDetail:(SocialConnectionDetail*)theSocialConnectionDetail;
//-(void)setShowSocialOptionsForViewController:(UIViewController*)theVC;

////////////////////////////////////////////////////

/**
* Note: Not called directly. Part of a feature request
**
 note: sc.pSocialConnectionDetail from caller needs to be an iVar to avoid exc bad access error from uias. arc issue if made a local var.
*
* show social options 2
* For communication purpose:
* currently supporting email, sms
* requires pSocialOptionsHostViewController to be set before calling
required props:
* self.pSubject = theSubject;
* self.pGeneralBodyMessage
* supports: feature request
*/
-(void)setShowCommunicationOptions;

////////////////////////////////////////////////////

/**
 * Note: Ok to call directly.
 * For communication purpose:
 * currently supporting email, sms
 required props:
 * self.pSubject = theSubject;
 * supports: feature request
 */
-(void) setShowCommunicationOptionsForViewController:(UIViewController*)theVC withRecipients:(NSArray*)theRecipients withSubject:(NSString*)theSubject withPromptMsg:(NSString*)thePromptMsg;

////////////////////////////////////////////////////
#pragma mark - Messages

/**
 * set full body msgs per shareType.
 * do concats here. Msgs will be used straight by selected communication option later.
 * msg formatting/concats is app dependant - should already be performed by caller in alloc.
 * used in social network/communication options.
 */
-(void) setMessageBodyForSocialNetworkAndCommunicationOption;

////////////////////////////////////////////////////

/**
 - caller adds feedback item to master list: pNsmaSocialConnectionFeedbackItemsMasterList
 - master list used to generate # of randomized feedback items
 */
-(void) setAddItemToSocialConnectionFeedbackItemsMasterList:(NSString*)theItem;

/** set up & prepare pNsmaSocialConnectionFeedbackItems for display
 * caller invokes this to access pNsmaSocialConnectionFeedbackItems
 */
-(void) setGenerateRandomSocialConnectionFeedbackItems;
////////////////////////////////////////////////////
#pragma mark - static methods

#pragma mark - TODO:- Triggered Tasks

/*
  deprecated.
 * check TriggeredTaskTarget status
 not exists ? init 1 for k.
 reached ? resets for k.
 not reached ? ++ for k.
 - on each TriggeredTaskTarget reached, hide additional user options. Only share option shown
 - resets after target is reached
 */
//+(BOOL) isTriggeredTaskTargetReachedForTriggeredTaskKey:(NSString*)theTriggeredTaskKey withTriggeredTaskTarget:(int)theTriggeredTaskTarget;
@end
