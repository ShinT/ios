//
//  SocialConnection.m
//  SocialConnection
//
//  Created by Shin Tan on 7/16/13.
//  Copyright (c) 2013 Shin Tan. All rights reserved.
//

/**
 * Desc:
 */

/**
 - TODO:
   - Add feature request props consts
  */

#import "SocialConnection.h"
#import "SocialConnectionVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
//required for sms kUTTypeMessage

//fb
//#import "FacebookSDK/FacebookSDK.h"
//#import "DEFacebookComposeViewController.h"
#import "Social/Social.h"
//#import "UIDevice+DEFacebookComposeViewController.h"

////////////////////////////////////////////////////
#pragma mark - implementation
@implementation SocialConnection {
    
    //social Connection
    SocialConnectionType selectedSocialConnectionType;
    
    //random indices store
    NSMutableArray* _gNsmaSocialConnectionFeedbackItemRandomIndexList;
    
    //current twitter, facebook sdk processing state
    SocialConnectionTypeStatus currentSocialConnectionTypeStatus;
    
    UIAlertView *alertViewInput;
    UIAlertView *alertViewShowOnly;
    UIAlertView *alertViewNoTwitter;
    UIAlertView *alertViewFacebookAfterStatusUpdate;
    UIActionSheet *uiasSocial;
    UIActionSheet *uiasCommunication;
}

////////////////////////////////////////////////////
#pragma mark - static const

//////////////////////////////////////////////////////
#pragma mark - delegate property synthesis

////////////////////////////////////////////////////
#pragma mark - class property synthesis

/* deprecated
@synthesize pSocialOptionsHostViewController,
pSocialOptionsMainMsg, pSubject
,pSelectedSocialConnectionType, pAppUrlForEmail,
pAppUrlForFacebook,pAppUrlForSMS,pAppUrlForTwitter
,pIsIMessage, pIsHTMLBasedEmail,pTestInSimulatorOnly,pSelectedSocialConnectionFeedbackItem,pNsmaSocialConnectionFeedbackItems,pNsmaSocialConnectionFeedbackItemsMasterList, pSocialConnectionFeedbackNumberOfItems, pShareType,  pShouldKeepSocialOptionsOpenAfterFinishedSharing, pRecipients;
*/
//properties for socialConnectionFeedbackVCM
/*
@synthesize pSocialConnectionFeedbackVCMBarButtonItemTitle,pSocialConnectionFeedbackVCMMainMsg,pSocialConnectionFeedbackVCMNavigationItemTitle;
*/
//properties - social
/*
@synthesize pEmailGeneralMessage,pFacebookGeneralMessage,pSMSGeneralMessage,pTwitterGeneralMessage;
*/
//properties - socialFanType - fb like, share, twitter follow
/*
@synthesize pSocialFanFacebookAppURL, pSocialFanFacebookLikeBoxURL, pSocialFanFacebookNavBackTitle, pSocialFanFacebookNavTitle, pSocialFanType;
*/

////////////////////////////////////////////////////
#pragma mark - public getters
/////////////////////////////////////////////////
#pragma mark - public setters

///////////////////////////////////////////////////
#pragma mark - private getters

-(NSMutableArray*) gNsmaSocialConnectionFeedbackItemRandomIndexList {
    if (_gNsmaSocialConnectionFeedbackItemRandomIndexList == nil) {
        _gNsmaSocialConnectionFeedbackItemRandomIndexList = [[NSMutableArray alloc]init];
    }
    return _gNsmaSocialConnectionFeedbackItemRandomIndexList;
}
///////////////////////////////////////////////////
#pragma mark - static gets

/* deprecated
+(NSString*)sgetTriggeredTaskKeySocialConnection {
    return kTriggeredTaskSocialConnection;
}

+(int) sgetTriggeredTaskTargetSocialConnection {
    return scTriggeredTaskTargetSocialConnection;
}
*/

////////////////////////////////////////////////////
#pragma mark - constructors

///1
-(id) init {
    if (self = [super init]) {
        //default props
        
        // no imsg. Send as pure sms. Seems to create prob otherwise.
        //self.pIsIMessage = NO;
        
        //default socialFanVC as facebook like for now.
        self.pSocialFanType = enumSocialFanTypeFacebookLike;
        
        self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl = @"";
        
    }
    return self;
}
//////////////////////////////////////////////////////////
#pragma mark - destructor

-(void) dealloc {
    //per ARC, obsolete: [super dealloc];
}

/////////////////////////////////////////////////////////
#pragma mark - public method implementation

/**
 - caller adds feedback item to master list: pNsmaSocialConnectionFeedbackItemsMasterList
 - master list used to generate # of randomized feedback items
 */
-(void) setAddItemToSocialConnectionFeedbackItemsMasterList:(NSString*)theItem {
    //late bind it
    if (self.pNsmaSocialConnectionFeedbackItemsMasterList == nil) {
        self.pNsmaSocialConnectionFeedbackItemsMasterList = [[NSMutableArray alloc]init];
    }
    [self.pNsmaSocialConnectionFeedbackItemsMasterList addObject:theItem];
}
/////////////////////////////////////////////////////
/* set up & prepare pNsmaSocialConnectionFeedbackItems for display
   result = pNsmaSocialConnectionFeedbackItems
 */
 -(void) setGenerateRandomSocialConnectionFeedbackItems {
     //set prop
     self.pNsmaSocialConnectionFeedbackItems = [self getRandomizedItemsFromSourceList:self.pNsmaSocialConnectionFeedbackItemsMasterList forNumberOfItems:self.pSocialConnectionDetail.pSocialConnectionFeedbackNumberOfItems withRandomIndexListForSourceList:[self gNsmaSocialConnectionFeedbackItemRandomIndexList]];
     
     //testing
     /*
     for (NSString *s in self.pNsmaSocialConnectionFeedbackItems) {
         dLog(@"setGenerateRandomSocialConnectionFeedbackItems - pNsmaSocialConnectionFeedbackItems: %@",s);
     }
      */
     //testing
}
/////////////////////////////////////////////////////
/*
 * init main share screen. SocialConnectionVC.
 * Home, My Thoughts, Feature Request
 * note: Host VC needs to subscribe to at least one delegate callback to avoid unrecognized selector sent to instance error b/c .delegate is set to source VC
 */
-(void) setShowShareOptionForViewController:(UIViewController *)theVC withSocialConnection:(SocialConnection*)theSocialConnection withSocialConnectionDetail:(SocialConnectionDetail*)theSocialConnectionDetail
 {
    //init share option sb
    UIStoryboard *sbMain = [UIStoryboard storyboardWithName:@"SocialConnection" bundle:nil];
    
    //init share option vc
    SocialConnectionVC *socialConnectionVC = [sbMain instantiateInitialViewController];
    
    //vc default props
    socialConnectionVC.pSocialConnection = theSocialConnection;
    socialConnectionVC.pSocialConnection.pSocialConnectionDetail = theSocialConnectionDetail;
    
    //delegate
    socialConnectionVC.delegate = theVC;
    
    socialConnectionVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    //show transparent, so need to add as childVC vs presentVC
    //[theVC presentViewController:socialConnectionVC animated:YES completion:^{}];
    
    //init
    /* bug: IB -attributes inspector - set wantsFullScreenLayout = YES for SocialConnectionVC;
     otherwise, there's a white gap on top screen in super view after adding as child VC & alpha is turned on
     */
    
    //using this method wont show transparency ? [theVC presentViewController:socialConnectionVC animated:YES completion:nil];
    
    [theVC addChildViewController:socialConnectionVC];
    [theVC.view addSubview:socialConnectionVC.view];
    [socialConnectionVC didMoveToParentViewController:theVC];

}

/////////////////////////////////////////////////////

/**
 * main connection point: email, sms, twitter, fb etc.
 * VC in pageviewcontroller needs to be parentViewController.
 
 */
/**
 * isFavingInSession - fav session. Track to help w/ caller viewAppear suppress/allow process.
 * email, sms & other social delegates trigger caller viewDisappear/Appear.
 * flag on isFavingInSession in showSocialNetwork for fav shareType.
 * flag off isFavingInSession in alertView dismiss - since sheet is always up after each social thread
 */
-(void)setShowSocialNetworkAndCommunicationOptionsForViewController:(UIViewController*)theVC withSocialConnectionDetail:(SocialConnectionDetail*)theSocialConnectionDetail {
    
    //safety check to make sure there's an active view in place 1st before triggering actionsheet view
    /*
    if (self.pSocialOptionsHostViewController.view == nil) {
       // dLog(@"setShowSocialOptions-host view: %@",self.pSocialOptionsHostViewController.view);
        
        return;
    }
    */
    
    //props
    
    //detail
    self.pSocialConnectionDetail = theSocialConnectionDetail;
    
    //prepare msg body for communication option
    [self setMessageBodyForSocialNetworkAndCommunicationOption];
    
    //set actionsheet host to VC param
    self.pUIActionSheetHostViewController = theVC;
    
    
    NSString *actionSheetTitle;
    
   // NSString *actionSheetTitle = @"Hey dear friend, \n\n Pls spread the love and tell your friends about us. \n Your support motivates us to work harder. \n\n +1 to you for helping us. Really appreciate it!";
    
    switch(self.pSocialConnectionDetail.pShareType) { //sharetype
        case enumShareTypeAppRecommendation: {
            //if user set actionsheet msg property, use that instead
            if (self.pSocialConnectionDetail.pSocialOptionActionSheetTitle != nil ) {
                actionSheetTitle = self.pSocialConnectionDetail.pSocialOptionActionSheetTitle; //pSocialOptionsMainMsg;
            }
            else {
                //use the template
                  actionSheetTitle = @"Hey dear friend, \n\n Pls pick any options below to spread the love & share with your friends. \n Your support motivates us to work harder. \n\n +1 to you for helping us :) Really appreciate it!";
            }
            break;
        }//app
        case enumShareTypeFave: {
              self.pIsSocialConnectionInSession = YES;
            if (self.pSocialConnectionDetail.pSocialOptionActionSheetTitle != nil ) {
                actionSheetTitle = self.pSocialConnectionDetail.pSocialOptionActionSheetTitle;
            }
            
            
            else {
                //use the template
                actionSheetTitle = @"Pick any options to share with your friends. \n You're also welcome back here to share with as many friends as you like anytime :)";
            }
            break;
        }
            
        //communication option
        case enumShareTypeCommunication: {
            //nothing else to do
            return;
            break;
        }
            
        default: {
            if (self.pSocialConnectionDetail.pSocialOptionActionSheetTitle != nil ) {
                actionSheetTitle = self.pSocialConnectionDetail.pSocialOptionActionSheetTitle;
            }
            else {
                //use the template
                actionSheetTitle = @"Pick any options to share with your friends. \n You're also welcome back here to share with as many friends as you like anytime :)";
            }

            break;
        }//default
            
    }//sharetype
    
  //UIActionSheet *uias
  uiasSocial = [[UIActionSheet alloc]initWithTitle:actionSheetTitle delegate:self cancelButtonTitle:@"<<" destructiveButtonTitle:nil
                            otherButtonTitles:@"SMS / Text", @"Email",@"Twitter",@"Facebook", nil];
    
    uiasSocial.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    
    /* set host VC prop
     * hostVC is now SocialConnectionFeedbackVCM
     * caller needs to set this
     * self.pHostViewController = theVC;
    */
    
    //1. if instantiate via ib segue & using only .view in showInView, actionsheet doesnt show & locks up the view? only shows if using .view.window. But, this is not a good solution.
    //remove IB segue. Instantiate VC manually, so .view is available
    //2. For VC in pageviewcontroller, theVC needs to be self.parentViewController
    [uiasSocial showInView:self.pUIActionSheetHostViewController.view];//
  //.window
    
    //ga
    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Show Social Options for Selected Feedback" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
    //pSelectedSocialConnectionFeedbackItem
    
    //ga real
    [PimpFitDataModel setGASendView:@"SocialConnection - Show Social Options"];
}

/////////////////////////////////////////////////////

/**
 * Note: Not called directly. Part of a feature request
 * For communication purpose:
 * currently supporting email, sms
 required props:
 * self.pSubject = theSubject;
 * self.pGeneralBodyMessage
 * supports: feature request
 */
-(void)setShowCommunicationOptions {
    
    //safety check to make sure there's an active view in place 1st before triggering actionsheet view
    /*
    if (self.pSocialOptionsHostViewController.view == nil) {
        // dLog(@"setShowSocialOptions-host view: %@",self.pSocialOptionsHostViewController.view);
        
        return;
    }
    */
    
    self.pSocialConnectionDetail.pSubject = [NSString stringWithFormat:@"%@ - %@",self.pSocialConnectionDetail.pSubject, self.pSocialConnectionDetail.pAppName];
    
    
    //share type
    self.pSocialConnectionDetail.pShareType = enumShareTypeCommunication;
    
        uiasCommunication = [[UIActionSheet alloc]initWithTitle:self.pSocialConnectionDetail.pSocialOptionActionSheetTitle delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil
                                            otherButtonTitles:@"SMS / Text", @"Email", nil];
    
    //pSocialOptionsMainMsg
    //@"<<" not used atm
    uiasCommunication.actionSheetStyle = UIActionSheetStyleDefault;//UIActionSheetStyleBlackTranslucent;

   
    //1. if instantiate via ib segue,  if using only .view in showInView, actionsheet doesnt show & locks up the view? only shows if using .view.window. But, this is not a good solution.
    //remove IB segue. Instantiate VC manually, so .view is available
    //set host
   
    //need to use .window here b/c initial SocialConnectionVC was loaded via addSubview vs presentVC to support transparency. This creates a prob w/ actionsheet if only using .view here
 
    [uiasCommunication showInView:self.pUIActionSheetHostViewController.view.window];
   
    //self.pSocialOptionsHostViewController.view
    //ga
    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Show Communication Options" withLabel:@"" withValue:nil];
    
    //ga real
    [PimpFitDataModel setGASendView:@"SocialConnection - Show Communication Options"];
}

//////////////////////////////////////
/**
 * Note: Ok to call directly.
 * For communication purpose:
 * currently supporting email, sms
 * requires pSocialOptionsHostViewController to be set before calling
 required props:
 * self.pSubject = theSubject;
 * self.pGeneralBodyMessage
 * supports: feature request
 * TODO:- needs refactor later
 */
-(void) setShowCommunicationOptionsForViewController:(UIViewController*)theVC withRecipients:(NSArray*)theRecipients withSubject:(NSString*)theSubject withPromptMsg:(NSString*)thePromptMsg {
    
    //test
    self.pIsSocialConnectionInSession = YES;
    
    self.pUIActionSheetHostViewController = theVC;
    self.pRecipients = theRecipients;
    self.pSocialConnectionDetail.pSubject = theSubject;
    self.pSocialConnectionDetail.pSocialOptionActionSheetTitle = thePromptMsg; //pSocialOptionsMainMsg
    
    self.pSocialConnectionDetail.pShareType = enumShareTypeCommunication;
    //self.pShareType = enumShareTypeCommunication;
    
    //self.delegate = self;
    [self setShowCommunicationOptions];
    
}

////////////////////////////////////////////////////
#pragma mark - Messages

/**
 * set full body msgs per shareType.
 * Msgs will be used straight by selected communication option later.
 * msg formatting/concats is app dependant - should already be performed by caller in alloc.
 * used in social network/communication options.
 */
-(void) setMessageBodyForSocialNetworkAndCommunicationOption {
    
    switch(self.pSocialConnectionDetail.pShareType) {
        case enumShareTypeFave: {
            self.pSocialConnectionDetail.pMsgBodyGeneralEmail = self.pSocialConnectionDetail.pMsgBodyFavEmail;
            
            self.pSocialConnectionDetail.pMsgBodyGeneralFacebook = self.pSocialConnectionDetail.pMsgBodyFavFacebook;
            
            self.pSocialConnectionDetail.pMsgBodyGeneralSMS  = self.pSocialConnectionDetail.pMsgBodyFavSMS;
            
            self.pSocialConnectionDetail.pMsgBodyGeneralTwitter = self.pSocialConnectionDetail.pMsgBodyFavTwitter;
            
            break;
        }
            
        case enumShareTypeCommunication: {
            //tbd
            break;
        }
        case enumShareTypeAppRecommendation: {
            self.pSocialConnectionDetail.pMsgBodyGeneralEmail = self.pSocialConnectionDetail.pMsgBodyAppRecommendationEmail;
            
            self.pSocialConnectionDetail.pMsgBodyGeneralFacebook = self.pSocialConnectionDetail.pMsgBodyAppRecommendationFacebook;
            
            self.pSocialConnectionDetail.pMsgBodyGeneralSMS  = self.pSocialConnectionDetail.pMsgBodyAppRecommendationSMS;
            
            self.pSocialConnectionDetail.pMsgBodyGeneralTwitter = self.pSocialConnectionDetail.pMsgBodyAppRecommendationTwitter;
            break;
        }
    }//s
}
////////////////////////////////////////////////////
#pragma mark - static method implementations

#pragma mark - Triggered Tasks

/* deprecated.
 - check completed cycle status
 not exists ? init 1 for k.
 reached ? resets for k.
 not reached ? ++ for k.
 - on each kCompletedCycle trigger reached, hide additional user options. Only share option shown
 - resets after kCompletedCycleTrigger is reached
 deprecated
 */
/*
+(BOOL) isTriggeredTaskTargetReachedForTriggeredTaskKey:(NSString*)theTriggeredTaskKey withTriggeredTaskTarget:(int)theTriggeredTaskTarget {
    BOOL reached = NO;
    
    //check current target status
    int currentTargetVal = [[NSUserDefaults standardUserDefaults]integerForKey:theTriggeredTaskKey];
    
    if (currentTargetVal == 0 ||currentTargetVal == -1) {
        //generate user defaults & set initial
        [[NSUserDefaults standardUserDefaults]setInteger:1 forKey:theTriggeredTaskKey];
        
        return reached;
        
    }//nothing yet - generate user defaults & set initial
    
    
    //user defaults already exists. Check status:
    if (currentTargetVal >= theTriggeredTaskTarget) {
        //hit trigger target or more. Reset
        [[NSUserDefaults standardUserDefaults]setInteger:0 forKey:theTriggeredTaskKey];
        //show additional options - ok to proceed
        reached = YES;
    }//
    else {
        //trigger point not reached yet - just increment
        [[NSUserDefaults standardUserDefaults]setInteger:currentTargetVal+1 forKey:theTriggeredTaskKey];
    }
    
    return reached;
          
}
*/
////////////////////////////////////////////////////
#pragma mark - private method implementation

#pragma mark - Email 1 - Social
/*
 * option 1
 * fullMsgBodyForEmail items: generalMsg + selectedFeedbackItem + appURL
 * MIME type JPEG: image/jpeg image/png
 */
//send email
-(void) setEmailShowSocialScreenForViewController:(UIViewController*)theVC withRecipients:(NSArray*)theRecipients withSubject:(NSString*)theSubject withGeneralMessageBody:(NSString*)theGeneralMessageBody isHTMLType:(BOOL) theIsHTMLType withEmailShortURLForApp:(NSString*)theEmailShortURLForApp withAttachmentData:(NSData*)theAttachmentData ofMimeType:(NSString*) theOfMimeType withFileName:(NSString*)theFileName {
   
    NSString *fullMsgBodyForEmail;
    
    //dataModel.gSocialConnection.pGeneralBodyMessage = [NSString stringWithFormat:@"%@ %@",dataModel.gSocialConnection.pGeneralBodyMessage,dataModel.gSocialConnection.pSelectedSocialConnectionFeedbackItem];
    
    //theIsHTMLType = NO; //not html-based - testing
    
    switch(theIsHTMLType) {
        case YES: {
            //setup general msg & construct app url w/ html <a> tag
            //note: \n wont work for html = true. use tag ver: <br/>
fullMsgBodyForEmail = [NSString stringWithFormat:@"%@ <br/> <br/> %@ <br/> <a href='%@'>%@</a>",theGeneralMessageBody,self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl,theEmailShortURLForApp,theEmailShortURLForApp];
            //self.pSelectedSocialConnectionFeedbackItem
                break;
        }//yes
            
        case NO: {
            //just show the general msg text & app url in str
            NSURL *urlEmail = [NSURL URLWithString:theEmailShortURLForApp];
            
            fullMsgBodyForEmail = [NSString stringWithFormat:@"%@ \n\n %@ \n %@",theGeneralMessageBody,self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl, urlEmail];
            //self.pSelectedSocialConnectionFeedbackItem
            break;
        }//no
    }

    //init email component
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc]init];
    
    //set delegate
    [mailVC setMailComposeDelegate:self];
    
    //send email if user has mail setup
    switch([MFMailComposeViewController canSendMail]) {
        
        case YES: {
          
            // set recipients
            if (theRecipients != nil) {
               [mailVC setToRecipients:theRecipients]; 
            }
            
            // set subj
            [mailVC setSubject:theSubject];
            
            // set msg body
            [mailVC setMessageBody:fullMsgBodyForEmail isHTML:theIsHTMLType];
            
            //set attachment
            if (theAttachmentData != nil) {
                [mailVC addAttachmentData:theAttachmentData mimeType:theOfMimeType fileName:theFileName];
            }
            
            
            //set style
            mailVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            
        
            //show
            [theVC presentViewController:mailVC animated:YES completion:nil];
            
            //ga
            
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - Show Email" withLabel:@"Count" withValue:nil];
            
        [PimpFitDataModel setGASendView:@"SocialConnection - Social Option - Show Email"];
            
            break;
        }// mail setup ok
            
        case NO: {
              [self setUIAlertViewShowWithTitle:@"Oops" withMsg:@"There's a bit of a problem in sending your email. Please check your device to make sure you're able to send email, then try again." withCancelBtnTitle:@"OK" withOtherBtnTitle:nil];
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - Email Error - Cannot Send Email - No Setup" withLabel:@"Email Error - Cannot Send Email - No Setup" withValue:nil];
            
            //ga
        [PimpFitDataModel setGASendView:@"SocialConnection - Social Option - Email Error - Cannot Send Email - No Setup"];
            
            break;
        }// mail not ok
            
    }//s
    
}
/////////////////////////////////////////////////////////
#pragma mark - Email 2 - General Communication

/*
 * option 2 - general email
 * fullMsgBodyForEmail
 */
-(void) setEmailShowGeneralCommunicationScreenForViewController:(UIViewController*)theVC withRecipients:(NSArray*)theRecipients withSubject:(NSString*)theSubject withGeneralMessageBody:(NSString*)theGeneralMessageBody isHTMLType:(BOOL) theIsHTMLType withAttachmentData:(NSData*)theAttachmentData ofMimeType:(NSString*) theOfMimeType withFileName:(NSString*)theFileName {
    
    NSString *fullMsgBodyForEmail;
    
    /*
    switch(theIsHTMLType) {
        case YES: {
            //setup general msg & construct app url w/ html <a> tag
            //note: \n wont work for html = true. use tag ver: <br/>
            fullMsgBodyForEmail = [NSString stringWithFormat:@"%@ <br/> <br/> %@ <br/> <a href='%@'>%@</a>",theGeneralMessageBody,self.pSelectedSocialConnectionFeedbackItem,theEmailShortURLForApp,theEmailShortURLForApp];
            break;
        }//yes
            
        case NO: {
            //just show the general msg text & app url in str
            NSURL *urlEmail = [NSURL URLWithString:theEmailShortURLForApp];
            
            fullMsgBodyForEmail = [NSString stringWithFormat:@"%@ \n\n %@ \n %@",theGeneralMessageBody,self.pSelectedSocialConnectionFeedbackItem, urlEmail];
            break;
        }//no
    }
    */
    
    //init email component
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc]init];
    
    //set delegate
    [mailVC setMailComposeDelegate:self];
    
    //send email if user has mail setup
    switch([MFMailComposeViewController canSendMail]) {
            
        case YES: {
            
            // set recipients
            if (theRecipients != nil) {
                [mailVC setToRecipients:theRecipients];
            }
            
            // set subj
            [mailVC setSubject:theSubject];
            
            // set msg body
            [mailVC setMessageBody:fullMsgBodyForEmail isHTML:theIsHTMLType];
            
            //set attachment
            if (theAttachmentData != nil) {
                [mailVC addAttachmentData:theAttachmentData mimeType:theOfMimeType fileName:theFileName];
            }
            
            
            //set style
            mailVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            
            
            //show
            [theVC presentViewController:mailVC animated:YES completion:nil];
            
            //ga
            
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Communication - Show Email" withLabel:@"Count" withValue:nil];
            
            [PimpFitDataModel setGASendView:@"SocialConnection - Communication Option - Show Email"];
            
            
            break;
        }// mail setup ok
            
        case NO: {
            [self setUIAlertViewShowWithTitle:@"Oops" withMsg:@"There's a bit of a problem in sending your email. Please check your device to make sure you're able to send email, then try again." withCancelBtnTitle:@"OK" withOtherBtnTitle:nil];
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - Email Error - Cannot Send Email - No Setup" withLabel:@"Email Error - Cannot Send Email - No Setup" withValue:nil];
            
            //ga
            [PimpFitDataModel setGASendView:@"SocialConnection - Communication Option - Email Error - Cannot Send Email - No Setup"];
            
            break;
        }// mail not ok
            
    }//s
    
}

/////////////////////////////////////////////////////////
# pragma mark - SMS 1 - Social

-(void) setSMSShowSocialScreenForViewController:(UIViewController*)theVC withRecipients:(NSArray*)theRecipients withGeneralMessageBody:(NSString*)theGeneralMessageBody isIMessage:(BOOL) theIsIMessage withSMSShortURLForApp:(NSString*)theSMSShortURLForApp withAttachmentData:(NSData*)theAttachmentData ofMimeType:(NSString*) theOfMimeType withAttachmentDataFileName:(NSString*)theAttachmentDataFileName {
    
    NSString *fullMsgBodyForSMS;
    
    /*
     NOTE: no html formatting for SMS. Not supported in MFMessageCompose
    */
    //set up as plain text only. Currently only gmail would highlight url
    NSURL *urlSMS = [NSURL URLWithString:theSMSShortURLForApp];
    
    //MsgBody props - includes textToShowBeforeUrl + URL
    fullMsgBodyForSMS = [NSString stringWithFormat:@"%@\n%@\n%@",theGeneralMessageBody,self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl, urlSMS];
    //self.pSelectedSocialConnectionFeedbackItem
    
    //theIsIMessage = NO; //not imsg - testing
    /*
    switch(theIsIMessage) {
        case YES: {
             //set up msg body as html /mms for iMessage
            // <a href = '' >link</a>
    
        fullMsgBodyForSMS = [NSString stringWithFormat:@"%@ <br/> %@ <br/> <a href='%@'>%@</a>",theGeneralMessageBody,self.pSelectedSocialConnectionFeedbackItem,theSMSShortURLForApp, theSMSShortURLForApp];
    
        break;
        }//y
        case NO: {
            //set up as plain text for sms only
            NSURL *urlSMS = [NSURL URLWithString:theSMSShortURLForApp];
            
            fullMsgBodyForSMS = [NSString stringWithFormat:@"%@\n %@\n %@",theGeneralMessageBody,self.pSelectedSocialConnectionFeedbackItem, urlSMS];
            break;
        }//n
    }
    */
    
    
    //init
    MFMessageComposeViewController *smsVC = [[MFMessageComposeViewController alloc]init];
    
    //set delegate
    [smsVC setMessageComposeDelegate:self];
    
    //send sms if user has setup
    switch ([MFMessageComposeViewController canSendText]) {
        case YES: {
            //set ps
            [smsVC setTitle:@"Hey, check this out!"];
            [smsVC setRecipients:theRecipients];
            [smsVC setBody:fullMsgBodyForSMS];
            
            //attachment data
            //note: kUTTypeMessage todo: <MobileCoreServices/MobileCoreServices.h>
            if (theAttachmentData) {
                NSString *kUTTypeVal;
                if ([theOfMimeType isEqualToString:cMIMETypePNG]) {
                    kUTTypeVal = (NSString*)kUTTypePNG;
                }
                else if ([theOfMimeType isEqualToString:cMIMETypeJPEG]) {
                    kUTTypeVal = (NSString*)kUTTypeJPEG;
                }
                
            [smsVC addAttachmentData:theAttachmentData typeIdentifier:kUTTypeVal filename:theAttachmentDataFileName];
            }
            
            //show
            [theVC presentViewController:smsVC animated:YES completion:nil];
            
            
            //ga
            
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - Show SMS" withLabel:@"Count" withValue:nil];
            
            [PimpFitDataModel setGASendView:@"SocialConnection - Social Option - Show SMS"];
            
            
            break;
        }//ok
            
        case NO: {
            //note:
            //bug: apple got its own error msg. sms no device setup error thrown by sms delegate causes exception if prompt another msg here. Skip for now
            currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusSMSNoDeviceSetup;
            
            //flag off to exit thread
            self.pIsSocialConnectionInSession = NO;
            /*
              [self setUIAlertViewShowWithTitle:@"Oops" withMsg:@"There's a bit of a problem in sending your text. Please check your device to make sure you're able to send SMS/Text, then try again." withCancelBtnTitle:@"OK" withOtherBtnTitle:nil];
             */
            
            
            
            
             //[self setUIAlertViewShowWithTitle:@"Oops" withMsg:@"There's a bit of a problem in sending your text. Please check your device to make sure you're able to send SMS/Text, then try again." withCancelBtnTitle:@"OK" withOtherBtnTitle:nil];
            
            //ga
           [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - SMS Error - Cannot Send SMS - No Setup" withLabel:@"SMS Error - Cannot Send SMS - No Setup" withValue:nil];
            
            //ga
            [PimpFitDataModel setGASendView:@"SocialConnection - Social Option - SMS Error - Cannot Send SMS - No Setup"];
            
            break;
        }//not ok
        
    }//s

}

/////////////////////////////////////////////////////////
# pragma mark - SMS 2 - Communication
-(void) setSMSShowGeneralCommunicationScreenForViewController:(UIViewController*)theVC withSubject:(NSString*)theSubject withRecipients:(NSArray*)theRecipients withGeneralMessageBody:(NSString*)theGeneralMessageBody isIMessage:(BOOL) theIsIMessage {
    
    NSString *fullMsgBodyForSMS;
    //theIsIMessage = NO; //not imsg - testing
    
    //init
MFMessageComposeViewController *smsVC = [[MFMessageComposeViewController alloc]init];
    
    //set delegate
    [smsVC setMessageComposeDelegate:self];
    
    //send sms if user has setup
    switch ([MFMessageComposeViewController canSendText]) {
        case YES: {
            //set ps
            [smsVC setTitle:theSubject];
            [smsVC setRecipients: theRecipients];
            [smsVC setBody:fullMsgBodyForSMS];
            
            //show
            [theVC presentViewController:smsVC animated:YES completion:nil];
            
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - Show SMS" withLabel:@"Count" withValue:nil];
            
            [PimpFitDataModel setGASendView:@"SocialConnection - Communication Option - Show SMS"];
            
            break;
        }//ok
            
        case NO: {
            //note:
            //bug: apple got its own error msg. sms no device setup error thrown by sms delegate causes exception if prompt another msg here. Skip for now
            currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusSMSNoDeviceSetup;
            
            //flag off to exit thread
            self.pIsSocialConnectionInSession = NO;
            
            /*
             [self setUIAlertViewShowWithTitle:@"Oops" withMsg:@"There's a bit of a problem in sending your text. Please check your device to make sure you're able to send SMS/Text, then try again." withCancelBtnTitle:@"OK" withOtherBtnTitle:nil];x`
             */
            
            //[self setUIAlertViewShowWithTitle:@"Oops" withMsg:@"There's a bit of a problem in sending your text. Please check your device to make sure you're able to send SMS/Text, then try again." withCancelBtnTitle:@"OK" withOtherBtnTitle:nil];
            
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - SMS Error - Cannot Send SMS - No Setup" withLabel:@"SMS Error - Cannot Send SMS - No Setup" withValue:nil];
            
            //ga
            [PimpFitDataModel setGASendView:@"SocialConnection - Communication Option - SMS Error - Cannot Send SMS - No Setup"];
            
            break;
        }//not ok
            
    }//s
    
}

/////////////////////////////////////////////////////////
#pragma mark - twitter - ios >6 <= 7

/**
 * For users w twitter setup on device.
 * if no setup, fallback to twitter via safari web sign in.
 * update: no app url or pMsgBodyTextToShowBeforeAppUrl.
 * update: add attachment img.
*/
-(void) setTwitterShowScreenForFrameworkInterface {
    switch ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
    case NO: {
        //device not setup for tweet. Fallback to web interface
        
        //ga
        [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Twitter Error - Cannot Send Tweet - No Setup" withLabel:@"Cannot Send Tweet-No Setup" withValue:nil];
        
        //1st: prompt reminder to set up twitter on device, then load fallback Web option in alertView delegate
        //reminder to set up twitter
        [self setUIAlertViewShowNoTwitterAlertWithTitle:@"Oops..Tweet Not posted" withMsg:@"Be sure to set up your Twitter by going to 'Settings' => 'Twitter' on your device." withCancelBtnTitle:nil withOtherBtnTitle:@"Okey Dokey"];
        
        //ga
        [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - Twitter Error" withLabel:@"Cannot Tweet - No Setup - Fallback to Web Interface" withValue:nil];
        
        //ga
        [PimpFitDataModel setGASendView:@"SocialConnection - Social Option - Twitter Error - Cannot Tweet - No Setup - Fallback to Web Interface"];
        
        //[self setTwitterShowScreenForFallbackOptionWebInterface];
        break;
    }
    
    case YES: {
        //twitter ok
        //init
        SLComposeViewController *tweetVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        //set props
        // set up tweet p's
        //set url using a separate method, per twitter sdk
        //concat msgBody
        
        //msg body
        //option 1:
        //for now, no app url or pMsgBodyTextToShowBeforeAppUrl in tweets.
        [tweetVC setInitialText:[NSString stringWithFormat:@"%@",self.pSocialConnectionDetail.pMsgBodyGeneralTwitter]];
        
        //option 2: w/ app url + pMsgBodyTextToShowBeforeAppUrl
        //[tweetVC setInitialText:[NSString stringWithFormat:@"%@ %@",self.pSocialConnectionDetail.pMsgBodyGeneralTwitter, self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
        
        //self.pSelectedSocialConnectionFeedbackItem
        
        //for now, no app url or pMsgBodyTextToShowBeforeAppUrl
        //[tweetVC addURL:[NSURL URLWithString:self.pSocialConnectionDetail.pAppUrlForTwitter]];
        
        //attachment
        [tweetVC addImage:self.pSocialConnectionDetail.pMsgAttachmentImage];
        
        //show - show this outside of handler block
        //hostVC = actionsheet host vc
        [self.pUIActionSheetHostViewController presentViewController:tweetVC animated:YES completion:nil];
        
        //block
        SLComposeViewControllerCompletionHandler blockHandler = ^(SLComposeViewControllerResult result) { //start block
            
            NSString *title;
            NSString *msg;
            
            //do cleanup for each outcome
            switch(result) {
                case SLComposeViewControllerResultCancelled: {
                    title = @"Tweet Not Posted";
                    switch(self.pSocialConnectionDetail.pShareType) { //share type
                        case enumShareTypeAppRecommendation: {
                            msg = @"You've chosen to cancel this tweet. Pls remember to hook your friends up later. They're waitin' on you :)";
                            
                            //@"You've chosen to cancel this tweet. Pls remember to share with your friends later. The more, the merrier :) \n \n We appreciate your support. Thanks!";
                            break;
                        }
                        case enumShareTypeFave: {
                            msg = @"You've chosen to cancel this tweet. Pls remember to hook your friends up later. They're waitin' on you :)";
                            break;
                        }
                            
                        default: {
                            msg = @"You've chosen to cancel this tweet. Pls remember to hook your friends up later. They're waitin' on you :)";
                            //@"You've chosen to cancel this tweet. Pls remember to share with your friends later. They're waitin' on you :)";
                            break;
                        }
                    }//share type
                    
                    
                    //set current state
                    currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusTwitterTWTweetSDKCancelled;
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Twitter Tweet Cancelled" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
                    //self.pSelectedSocialConnectionFeedbackItem
                    
                    //all done - dismiss TWTweet interface
                    // have to use pHost b/c only weak prop accepted inside block w/out compiler warning?
                    //hostVC = actionsheet host vc
                    [self.pUIActionSheetHostViewController dismissViewControllerAnimated:YES completion:nil];
                    
                    break;
                }//c-c
                    
                case SLComposeViewControllerResultDone: {
                    title = @"Tweet Successfully Posted!";
                    switch(self.pSocialConnectionDetail.pShareType) {//sharetype
                        case enumShareTypeAppRecommendation: {
                            msg = @"Sweet! We really appreciate your shout-out. \n Thanks again. Peace.";
                            break;
                        }
                        case enumShareTypeFave: {
                            msg = @"Sweet! Keep up the good work, champion. Peace.";
                            break;
                        }
                        default: {
                            msg = @"Sweet! Keep up the good work, champion. Peace.";
                            break;
                        }
                    }//sharetype
                    
                    
                    //set current state
                    currentSocialConnectionTypeStatus =enumSocialConnectionTypeStatusTwitterTWTweetSDKDone;
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Twitter Tweet Sent!" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    
                    //ga real - twitter sent
                    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection-Twitter Tweet Sent!-%@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl ]];
                    
                    //self.pSelectedSocialConnectionFeedbackItem
                    //all done - dismiss TWTweet interface
                    // have to use pHost b/c only weak prop accepted inside block w/out compiler warning?
                    [self.pUIActionSheetHostViewController dismissViewControllerAnimated:YES completion:nil];
                    
                    break;
                }//c-done
            } //s
            
            //show tweet outcome
            [self setUIAlertViewShowWithTitle:title withMsg:msg withCancelBtnTitle:nil withOtherBtnTitle:@"Okey Dokey"];
            
        }; //block
        
        //assign handler completion Block to VC
        tweetVC.completionHandler = blockHandler;
        
        break;
    }//c- yes - ok to tweet
            
 }//s
}
/////////////////////////////////////////////////////////
# pragma mark - twitter - via ios=5 framework

/*
 - For users w twitter setup on device
 - if no setup, fallback to twitter via safari web sign in.
 - obsolete - ios 7
 */
/*
-(void) setTwitterShowScreenForFrameworkInterface {
    
    
    TWTweetComposeViewController *tweetVC = [[TWTweetComposeViewController alloc]init];
    
    //check tweet setup on device
  
    switch([TWTweetComposeViewController canSendTweet]) {
        case YES: { //yes - ok to use TWTweet
            
            
            // set up tweet p's
            //set url using a separate method, per twitter sdk
            //concat msgBody
            [tweetVC setInitialText:[NSString stringWithFormat:@"%@ %@",self.pTwitterGeneralMessage, self.pSelectedSocialConnectionFeedbackItem]];
            
            [tweetVC addURL:[NSURL URLWithString:self.pTwitterShortURLForApp]];
            
            //show - show this outside of handler block
            [self.pSocialOptionsHostViewController presentViewController:tweetVC animated:YES completion:nil];
            
            
            //block - after-user-complete-tweet event handler (alternative to delegate method)
            tweetVC.completionHandler = ^(TWTweetComposeViewControllerResult tweetResult) {// start block
                
                NSString *title;
                NSString *msg;
                
                //do cleanup for each outcome
                switch(tweetResult) {
                    case TWTweetComposeViewControllerResultCancelled: {//c
                        title = @"Tweet Not Posted";
                        switch([self pShareType]) { //share type
                            case enumShareTypeAppRecommendation: {
                                msg = @"You've chosen to cancel this tweet. Pls remember to share with your friends later. The more, the merrier :) \n \n We appreciate your support. Thanks!";
                                break;
                            }
                            case enumShareTypeStatusUpdate: {
                                msg = @"You've chosen to cancel this tweet. Pls remember to share with your friends later. They're waitin' on you :)";
                                break;
                            }
                                
                            default: {
                                msg = @"You've chosen to cancel this tweet. Pls remember to share with your friends later. They're waitin' on you :)";
                                break;
                            }
                        }//share type
                        
                        
                        //set current state
                        currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusTwitterTWTweetSDKCancelled;
                        
                        //ga
                        [DataModel setGASendEventWithCategory:@"SocialConnection-Social Options" withAction:@"Twitter Tweet Cancelled" withLabel:self.pSelectedSocialConnectionFeedbackItem withValue:nil];
                        
                        //all done - dismiss TWTweet interface
                        // have to use pHost b/c only weak prop accepted inside block w/out compiler warning?
                        [self.pSocialOptionsHostViewController dismissViewControllerAnimated:YES completion:nil];
                        
                        
                        
                        break;
                    }//c
                        
                    case TWTweetComposeViewControllerResultDone: {
                        title = @"Tweet Successfully Posted!";
                        switch([self pShareType]) {//sharetype
                            case enumShareTypeAppRecommendation: {
                                msg = @"Sweet! We really appreciate your support. \n Thanks again, dear friend. Peace.";
                                break;
                            }
                            case enumShareTypeStatusUpdate: {
                                msg = @"Sweet! Keep up the good work, champion. Peace.";
                                break;
                            }
                            default: {
                                msg = @"Sweet! Keep up the good work, champion. Peace.";
                                break;
                            }
                        }//sharetype
                        
                        
                        //set current state
                        currentSocialConnectionTypeStatus =enumSocialConnectionTypeStatusTwitterTWTweetSDKDone;
                        
                        //ga
                        [DataModel setGASendEventWithCategory:@"SocialConnection-Social Options" withAction:@"Twitter Tweet Sent!" withLabel:self.pSelectedSocialConnectionFeedbackItem withValue:nil];
                        
                        //all done - dismiss TWTweet interface
                        // have to use pHost b/c only weak prop accepted inside block w/out compiler warning?
                        [self.pSocialOptionsHostViewController dismissViewControllerAnimated:YES completion:nil];
                        
                        break;
                    }//d
                }//s
                
                
                //show tweet outcome
                [self setUIAlertViewShowWithTitle:title withMsg:msg withCancelBtnTitle:nil withOtherBtnTitle:@"Okey Dokey"];
                
            }; //result block end
            break;
            
        }//yes - ok to use TWTweet
            
        case NO: {
            //device not setup for tweet. Fallback to web interface
            
            //ga
            [DataModel setGASendEventWithCategory:@"SocialConnection-Social Options" withAction:@"Twitter Error" withLabel:@"Cannot Send Tweet-No Setup" withValue:nil];
            
            //1st: prompt reminder to set up twitter on device, then load fallback Web option in alertView delegate
            //reminder to set up twitter
            [self setUIAlertViewShowNoTwitterAlertWithTitle:@"Oops..Tweet Not posted" withMsg:@"Be sure to set up your Twitter by going to 'Settings' => 'Twitter' on your device." withCancelBtnTitle:nil withOtherBtnTitle:@"Okey Dokey"];
            
            //[self setTwitterShowScreenForFallbackOptionWebInterface];
           
            break;
        }//n
            
    }//s
}
 */
/////////////////////////////////////////////////////////
# pragma mark - twitter - via web interface

/**
 * fallback option for twitter via safari web sign in.
 * For users w/out twitter setup on device
 * update: web interface: ok to add app url + pMsgBodyTextToShowBeforeAppUrl since there's no option for img
 */
-(void) setTwitterShowScreenForFallbackOptionWebInterface {
    //set status
    currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusTwitterFallbackWebInterface;
    
    //show instruction
    //option 1:
    //for now, no app url or pMsgBodyTextToShowBeforeAppUrl
   // NSString *msgBody =[NSString stringWithFormat:@"%@",self.pSocialConnectionDetail.pMsgBodyGeneralTwitter];
    
    //option 2: w/ app url + pMsgBodyTextToShowBeforeAppUrl
    
    NSString *msgBody = [NSString stringWithFormat:@"%@",self.pSocialConnectionDetail.pMsgBodyGeneralTwitter];
    
    //for now, no app url or pMsgBodyTextToShowBeforeAppUrl in tweets.
    //[NSString stringWithFormat:@"%@ %@ %@",self.pSocialConnectionDetail.pMsgBodyGeneralTwitter,self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl,self.pSocialConnectionDetail.pAppUrlForTwitter];
    
    [self setUIAlertViewShowInputWithTitle:nil withMsg:@"For now, pls sign in to your Twitter in the next screen. Then, paste the already copied text below to your tweet to share. That's it. Thanks!" withDefaultInputText:msgBody withOKBtnTitle:@"Twitter"];
    
    //self.pSelectedSocialConnectionFeedbackItem
    
    //ga
    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Twitter Fallback Web Interface" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
    //pSelectedSocialConnectionFeedbackItem
    
    //ga real twitter - web interface
    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection-Twitter Web Interface-%@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
    //self.pSelectedSocialConnectionFeedbackItem
    //send to safari twitter login section in UIAlertView delegate after ok
    
}
/////////////////////////////////////////////////////////
# pragma mark - facebook

/*
 * update: SLCompose support
 */
 -(void) setFacebookShowScreenForViewController {
     NSString *title;
     NSString *msg;
     
     switch([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
     case NO: {
         //device not setup for fb
         title = @"No Facebook Setup On Device";
         msg = @"Be sure to set up your Facebook by going to 'Settings' => 'Facebook' on your device. Then, try posting again.\n\n Pls remember to hook your friends up later. They're waitin' on you :)";
         
         //set current state
         currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusFacebookNoDeviceSetup;
         
         //prompt
         [self setUIAlertViewShowWithTitle:title withMsg:msg withCancelBtnTitle:nil withOtherBtnTitle:@"Okey Dokey"];
         
         //ga
          [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Facebook Status - FallbackWebInterface" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
         
         [PimpFitDataModel setGASendView:@"SocialConnection - Social Options - Facebook - No Setup - Fallback to Web Interface"];
         //self.pSelectedSocialConnectionFeedbackItem
         
         
     //device not setup for fb
         break;
     }//c- no
        
        
    case YES: {
        //device setup for fb
        //init
        SLComposeViewController *fbVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        //props - includes textToShowBeforeUrl + URL
        [fbVC setInitialText:[NSString stringWithFormat:@"%@ %@",self.pSocialConnectionDetail.pMsgBodyGeneralFacebook, self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
        
        //note: addURL - url embedded at eof of msgbody 
        [fbVC addURL:[NSURL URLWithString:self.pSocialConnectionDetail.pAppUrlForFacebook]];
        
        //attachment
        [fbVC addImage:self.pSocialConnectionDetail.pMsgAttachmentImage];
        
        //show - outside of block
        [self.pUIActionSheetHostViewController presentViewController:fbVC animated:YES completion:nil];
        
        //block
        SLComposeViewControllerCompletionHandler bHandler = ^(SLComposeViewControllerResult result) {//start block
            NSString *title;
            NSString *msg;

            switch(result) {
                case SLComposeViewControllerResultCancelled: {
                    //x
                   title = @"Facebook Posting Cancelled!";
                   msg = @"Pls remember to hook your friends up later. They're waitin' on you :)";
                    
                    //set current state
                    currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusFacebookCancelled;
                    
                    //ga
                     [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Facebook Status - FallbackWebInterface" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    //device not setup for fb

                    break;
                }//c-c
                    
                case SLComposeViewControllerResultDone: {
                    title = @"Facebook Status Successfully Posted!";
                    
                    switch(self.pSocialConnectionDetail.pShareType) { //share type
                        case enumShareTypeAppRecommendation: {
                            msg = @"Sweet! We really appreciate your shout-out.\n Thanks again. Peace.";
                            break;
                        }
                        case enumShareTypeFave: {
                            msg = @"Sweet! Keep up the good work, champion. Peace.";
                            break;
                        }
                        default: {
                            msg = @"Sweet! Keep up the good work, champion. Peace.";
                            break;
                        }
                    } //share type
                    
                    //set current state
                    currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusFacebookDone;
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Facebook Status Sent!" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    
                    //ga real - fb sent
                     [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Facebook Status Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
//pSelectedSocialConnectionFeedbackItem
                    break;
                }//c-d
            }//s-result
            
            //show fb outcome
            if ([title isEqualToString:@"Facebook Status Successfully Posted!"]) {
                
                //TODO:- bug: SocialFanVC dismiss causes caller viewAppear to refresh contents
                
                // prompt w/ option to fb like
                [self setUIAlertViewShowOptionsForFacebookAfterStatusUpdateWithTitle:title withMsg:msg withOKBtnTitle:@"<<" withOtherBtnTitle:@"Like!" ];
             }
            else {
                //just a regular prompt
                   [self setUIAlertViewShowWithTitle:title withMsg:msg withCancelBtnTitle:nil withOtherBtnTitle:@"Okey Dokey"];
            }
            

        
            
        };//end block
        
        //assign handler completion Block to VC
        fbVC.completionHandler = bHandler;
        
        //device setup for fb
        break;
    }//c-y
    
  }//s
     
 }

/////////////////////////////////////////////////////////
# pragma mark - fb - via web interface

/*
 - fallback option for fb via safari web sign in.
 - For users w/out fb setup on device
 */
-(void) setFacebookShowScreenForFallbackOptionWebInterface {
    //set status
    currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusFacebookFallbackWebInterface;
    
    //show instruction
    [self setUIAlertViewShowInputWithTitle:nil withMsg:@"For now, pls sign in to your Facebook in the next screen. Then, paste the already copied text below to your post. That's it. Thanks!" withDefaultInputText:[NSString stringWithFormat:@"%@ %@ %@",self.pSocialConnectionDetail.pMsgBodyGeneralFacebook,self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl,self.pSocialConnectionDetail.pAppUrlForFacebook] withOKBtnTitle:@"Facebook"];
    
    //self.pSelectedSocialConnectionFeedbackItem
    
    //ga
    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Facebook Fallback Web Interface" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
    
    //pSelectedSocialConnectionFeedbackItem
    
    //ga real twitter - web interface
[PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Facebook Web Interface - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
    //pSelectedSocialConnectionFeedbackItem
    //send to safari twitter login section in UIAlertView delegate after ok
    
}
/////////////////////////////////////////////////////////

/*
 deprecated.
-(void) setFacebookShowScreenForViewController {
    
    //init
    DEFacebookComposeViewController *facebookViewComposer = [[DEFacebookComposeViewController alloc] init];
   
 
     * init DEFacebook obj, then set ptr to a weak reference, then use the weak reference ptr, including in the block
     * otherwise, attempt to dismiss this VC w/ [facebookViewComposer dismissVC] causes "retain cycle" error
      . Block will not allow strong or iVar ref, only weak ref
 
    __weak DEFacebookComposeViewController *weakFaceBookViewComposer = facebookViewComposer;
    
    // If you want to use the Facebook app with multiple iOS apps you can set an URL scheme suffix
    //    facebookViewComposer.urlSchemeSuffix = @"facebooksample";
    
    //set style
    weakFaceBookViewComposer.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    
    //set msg concat sans the url
    [weakFaceBookViewComposer setInitialText:[NSString stringWithFormat:@"%@ %@",self.pFacebookGeneralMessage,pSelectedSocialConnectionFeedbackItem]];
    
    // set url separately per sdk
    [weakFaceBookViewComposer addURL:[NSURL URLWithString:self.pFacebookShortURLForApp]];
    
    // add image - optional
    //[facebookViewComposer addImage:[UIImage imageNamed:@"2.jpg"]];
    
    [weakFaceBookViewComposer setCompletionHandler:^(DEFacebookComposeViewControllerResult result) { //handler block start
        NSString *title;
        NSString *msg;
        
        switch (result) {
    
            case DEFacebookComposeViewControllerResultCancelled:
                title = @"Oops..Facebook Status Not Posted";
                
                switch([self pShareType]) { //share type
                    case enumShareTypeAppRecommendation: {
                        msg = @"Be sure to set up your Facebook by going to 'Settings' => 'Facebook' on your device. Then, try posting your status again.\n\n Pls remember to share with your friends later. The more, the merrier :)";
                        break;
                    }
                    case enumShareTypeStatusUpdate: {
                        msg = @"Be sure to set up your Facebook by going to 'Settings' => 'Facebook' on your device. Then, try posting your status again.\n\n Pls remember to share with your friends later. They're waitin' on you :)";
                        break;
                    }
                    default: {
                         msg = @"Be sure to set up your Facebook by going to 'Settings' => 'Facebook' on your device. Then, try posting your status again.\n\n Pls remember to share with your friends later. They're waitin' on you :)";
                        break;
                    }
                }//share type
                
                
                //set current state
                currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusFacebookDECancelled;
                
                //ga
                [DataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Facebook Status Cancelled" withLabel:self.pSelectedSocialConnectionFeedbackItem withValue:nil];
                
                break;
                
            case DEFacebookComposeViewControllerResultDone:
                title = @"Facebook Status Successfully Posted!";
                
                switch([self pShareType]) { //share type
                    case enumShareTypeAppRecommendation: {
                        msg = @"Sweet! We really appreciate your support.\n Thanks again, dear friend. Peace.";
                        break;
                    }
                    case enumShareTypeStatusUpdate: {
                        msg = @"Sweet! Keep up the good work, champion. Peace.";
                        break;
                    }
                    default: {
                        msg = @"Sweet! Keep up the good work, champion. Peace.";
                        break;
                    }
                } //share type
                
                //set current state
                currentSocialConnectionTypeStatus = enumSocialConnectionTypeStatusFacebookDEDone;
                
                //ga
                //[DataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Facebook Status Sent!" withLabel:self.pSelectedSocialConnectionFeedbackItem withValue:nil];
                
                //ga real - fb sent
               // [DataModel setGASendView:[NSString stringWithFormat:@"SocialConnection-Facebook Status Sent! -%@",self.pSelectedSocialConnectionFeedbackItem]];
                
                break;
        }
        
        //close fb view
        //[weakFaceBookViewComposer dismissModalViewControllerAnimated:YES];
        [weakFaceBookViewComposer dismissViewControllerAnimated:YES completion:nil];
        
        // [self.pSocialOptionsHostViewController dismissModalViewControllerAnimated:YES];
   
         * cannot dismiss w/ hostVC b/c facebook sdk bug: if user does not have facebook setup on device, the fb sdk triggered alertView - cancel option will dismiss the hostView, then causes setShowSocialOptions - [uias showInView:self.pSocialOptionsHostViewController.view] to stop working 
             b/c self.pSocialOptionsHostViewController.view = nil. The view was released by the facebook SDK!
         . Block will not allow strong or iVar ref, only weak ref. Releasing w/ [weakFaceBookViewComposer dismissModalViewControllerAnimated:YES] solves this bug.
        
       
       
        
        //show post outcome
       //testing fb like
        //if ([title isEqualToString:@"Facebook Status Successfully Posted!"]) {
            // prompt w/ option to fb like
            [self setUIAlertViewShowOptionsForFacebookAfterStatusUpdateWithTitle:title withMsg:msg withOKBtnTitle:@"<<" withOtherBtnTitle:@"Like!" ];
       // }
         //   else {
                //just a regular prompt
           //     [self setUIAlertViewShowWithTitle:title withMsg:msg withCancelBtnTitle:nil withOtherBtnTitle:@"Okey Dokey"];
            //}
       // }
        
        // [self setUIAlertViewShowWithTitle:title withMsg:msg withCancelBtnTitle:nil withOtherBtnTitle:@"Okey Dokey"];
        
        
    }]; //handler block end
    
    
    [self.pSocialOptionsHostViewController presentViewController:weakFaceBookViewComposer animated:YES completion:nil];
    //deprecated [self.pSocialOptionsHostViewController presentModalViewController:weakFaceBookViewComposer animated:YES];
    
    //no longer require for ios >= 7

    if ([UIDevice de_isIOS4]) {
        [self.pSocialOptionsHostViewController presentModalViewController:weakFaceBookViewComposer animated:YES];
 
    } else {
        [self.pSocialOptionsHostViewController presentViewController:weakFaceBookViewComposer animated:YES completion:^{}];
    }

    
    //[facebookViewComposer release];
   
}
*/
/////////////////////////////////////////////////////////
#pragma mark - facebook like box url

-(void) setFacebookShowLikeView {
    SocialFanVC *socialFanVC = [[SocialFanVC alloc]init];
    
    socialFanVC.pNavTitle = self.pSocialConnectionDetail.pFacebookLikeNavMainTitle;//self.pSocialFanFacebookNavTitle;
    socialFanVC.pNavBackTitle = self.pSocialConnectionDetail.pFacebookLikeNavBackTitle; //pSocialFanFacebookNavBackTitle;
    socialFanVC.pURLOfFacebookApp = self.pSocialConnectionDetail.pFacebookAppUrl;//pSocialFanFacebookAppURL;
    socialFanVC.pURLOfFacebookLikeBox = self.pSocialConnectionDetail.pFacebookLikeBoxUrl; //pSocialFanFacebookLikeBoxURL;
    socialFanVC.pSocialFanType = self.pSocialFanType;
    
    [self.pUIActionSheetHostViewController presentViewController:socialFanVC animated:YES completion:nil];
    
}
/////////////////////////////////////////////////////////
#pragma mark - check facebook like

//Not in use. TODO:- need testing
/*
-(void) setFacebookCheckLike{
    
    FBRequestConnection *newConnection = [[FBRequestConnection alloc] init] ;
    FBRequest *request = [[FBRequest alloc] initWithSession:FBSession.activeSession
                                                  graphPath:@"me/likes"
                                                 parameters:[NSMutableDictionary dictionary]
                                                 HTTPMethod:@"GET"];
    
    [newConnection addRequest:request completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (error)
        {
            NSLog(@"error %@", result);
        } else {
            BOOL liked = NO;
            NSLog(@"result %@", result);
            if ([result isKindOfClass:[NSDictionary class]]){
                NSArray *likes = [result objectForKey:@"data"];
                
                for (NSDictionary *like in likes) {
                    if ([[like objectForKey:@"id"] isEqualToString:@"__page_id__"]) {
                        NSLog(@"like");
                        liked = YES;
                        break;
                    }
                }
            }
            
            if (!liked) {
                if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fb://page/__page_id__"]]) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"fb://page/__page_id__"]];
                } else {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@""]];
                }
            }
        };
    }];
    
    [newConnection start];
    
    // [request release];
    
}
 */
////////////////////////////////////////////////////////
#pragma mark - random component

/*
 * TODO: Requires a nsma random index list for source list getter
 * generic version for random. Source store/list could be specified at run time
 *replacement for getRandomIndexForMaxItemCountOf:(int)highIndex
 * 1st time, empty indices: just randomize & store in index store
 * once reaches capacity: reset store 1st, then add 1st fresh rand
 * other cases: randomize, check against index store, then add to store if not found. Keep iterating, until found new index
 * theSourceList - master list of items
 * highIndex - maxItemCountOf master list
 * theRandomIndexList - randomIndex store to use for housing used indices for this master list
 *RETURNS a usable randomized index
 * TODO:- This algorithm needs to be revisited. For now, it's ok
 */
-(int) getRandomItemIndexFromSourceList:(NSMutableArray*)theSourceList  withRandomIndexListForSourceList:(NSMutableArray*)theRandomIndexListForSourceList {
    
    //int highIndex = theMaxSourceIndex; //default to 12 - more possibilities
    int randIndex = 0;
    
    if ([theRandomIndexListForSourceList count] == 0) {
        // index store at start point
        //just randomize against high index
        randIndex = arc4random() % [theSourceList count];
        
        //store in random index list
        [theRandomIndexListForSourceList addObject:[NSString stringWithFormat:@"%i",randIndex]];
       // dLog(@"getRandomItemIndexFromSourceList- case 0- added rand:%i",randIndex);
    }//0
    
    
    else if ([theRandomIndexListForSourceList count]== [theSourceList count]) {
        //index store reach cap
       // dLog(@"getRandomItemIndexFromSourceList - case max-reach cap - clear store");
        
        //reset index store 1st
        [theRandomIndexListForSourceList removeAllObjects];
        
        //start fresh - randIndex
        randIndex = arc4random() % [theSourceList count];
        
        //check for exist - continue looping if index exist until found 1
        /* no need since start fresh
        while ([self getRandomDoesIndexExist:rand forRandomIndexList:theRandomIndexListForSourceList]) {
            rand = arc4random() % [theSourceList count];
            dLog(@"getRandomItemIndexFromSourceList - case max-reach cap- found rand:%i",rand);
        }
        */
        
        //store the above rand as 1st fresh obj
        [theRandomIndexListForSourceList addObject:[NSString stringWithFormat:@"%i",randIndex]];
        //dLog(@"getRandomItemIndexFromSourceList - case max-reach cap - added rand as 1st fresh:%i",randIndex);
        
    }
    
    else {
        //rand,then check for existence
        randIndex = arc4random() % [theSourceList count];
        while([self getRandomDoesIndexExist:randIndex forRandomIndexList:theRandomIndexListForSourceList]) {
            randIndex = arc4random() % [theSourceList count];
           // dLog(@"getRandomItemIndexFromSourceList - else- grab rand:%i",randIndex);
        }
        
        //usable rand found. Add to index store
        [theRandomIndexListForSourceList addObject:[NSString stringWithFormat:@"%i",randIndex]];
      //  dLog(@"getRandomItemIndexFromSourceList - else - added to index rand:%i",randIndex);
        
    }// random index store is ongoing
    
    
    return randIndex;
}
//////////////////////////////////////////////

/*
 * retrieves an item from sourceList store randomly
 *w/ randIndex - getRandomItemIndexFromSourceList
 */
-(id) getRandomItemFromSourceList:(NSMutableArray*)theSourceList withRandomIndexListForSourceList:(NSMutableArray*)theRandomIndexListForSourceList {
    
    id randomItem = nil;
    if ([theSourceList count]>0) {
        
        //algorithm 1
        // int random = arc4random() % self.pExerciseCount; //0 based index
        
        //algorithm 2 - seems to be repeating too often?
        //int random = arc4random_uniform(self.pExerciseCount);
        
        //algorithm 3 - random + check existing index store
        //get a usable index
        int randIndex = [self getRandomItemIndexFromSourceList:theSourceList  withRandomIndexListForSourceList:theRandomIndexListForSourceList];
        //dLog(@"getRandomItemFromSourceList - r:%i",randIndex);
        
        //safety check
        if (randIndex >= [theSourceList count]) {
           // dLog(@"getRandomItemFromSourceList- r prob:%i",randIndex);
            randIndex = 0; //set to something just in case
        }
        
        //get the random item in ques
        randomItem = [theSourceList objectAtIndex:randIndex];
        //dLog(@"getRandomItemFromSourceList- %@",randomItem);
       // dLog(@"%@",@"---------------------------------------");
    }
    
    return randomItem;
}

//////////////////////////////////////////////////////////////

/*
 * check index existence in index store given rand
 * result:yes found index existence 
 */
-(BOOL) getRandomDoesIndexExist:(int)theIndex forRandomIndexList:(NSMutableArray*)theRandomIndexList {
    int i = 0;
    BOOL result = NO;
    while (i<[theRandomIndexList count]) {
        if ([[theRandomIndexList objectAtIndex:i]intValue]== theIndex) {
            //exists, get out
           // dLog(@"getRandomDoesIndexExist - found existing rand:%i = theIndex:%i",[[theRandomIndexList objectAtIndex:i]intValue],theIndex);
            result = YES;
            break;
        }
        i++;
    }
    return result;
}
//////////////////////////////////////////////////////////////

/*
 * randomly retrieve & set items from source store to target store, given item count
 * builds a target list w/ randomized items from source list
 * theRandomIndexList - randomIndex store pertaining to sourceList
 * implements getRandomItemFromSourceList w/ random algorithm 3
 */
//check randomIndexList to use
/*
 NSMutableArray *randomIndexList;
if (theSourceList == self.pNsmaSocialConnectionFeedbackItemsMasterList) {
    randomIndexList = [self nsmaSocialConnectionFeedbackItemRandomIndexList];
}
*/

-(NSMutableArray*) getRandomizedItemsFromSourceList:(NSMutableArray*)theSourceList forNumberOfItems:(int)theNumberOfItems withRandomIndexListForSourceList:(NSMutableArray*)theRandomIndexList {
    NSMutableArray *targetList;
    if ([theSourceList count]>0) {
               
        //init
        targetList = [[NSMutableArray alloc]initWithCapacity:theNumberOfItems];
        
        //load up the target list w/ usable randoms
        for (int i = 0; i < theNumberOfItems; i++) {
            [targetList addObject:[self getRandomItemFromSourceList:theSourceList withRandomIndexListForSourceList:theRandomIndexList]];
          //  dLog(@"setRandomPickItemsFromSourceList - theTargetList: %@",[targetList objectAtIndex:i]);
        }//f
        
    }//[theSourceList count]
    return targetList;
}

/////////////////////////////////////////////////////////
#pragma mark - View Tasks

-(void) setUIAlertViewShowWithTitle:(NSString*)title withMsg:(NSString*)msg withCancelBtnTitle:(NSString*)cancelBtnTitle withOtherBtnTitle:(NSString*) otherBtnTitle {
    
    alertViewShowOnly = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:cancelBtnTitle otherButtonTitles:otherBtnTitle, nil];
    
    [alertViewShowOnly show];
}
//////////////////////////////////////////////////////////
#pragma mark - prompt no twitter alertView

-(void) setUIAlertViewShowNoTwitterAlertWithTitle:(NSString*)theTitle withMsg:(NSString*)theMsg withCancelBtnTitle:(NSString*)cancelBtnTitle withOtherBtnTitle:(NSString*) otherBtnTitle {
    alertViewNoTwitter = [[UIAlertView alloc]initWithTitle:theTitle message:theMsg delegate:self cancelButtonTitle:cancelBtnTitle otherButtonTitles:otherBtnTitle, nil];
    
    [alertViewNoTwitter show];
}
///////////////////////////////////////////////////////////
#pragma mark - prompt facebook after status update

/**additional option to like after status update */
-(void) setUIAlertViewShowOptionsForFacebookAfterStatusUpdateWithTitle:(NSString*)theTitle withMsg:(NSString*)theMsg withOKBtnTitle:(NSString*)okBtnTitle withOtherBtnTitle:(NSString*) otherBtnTitle   {
    
    alertViewFacebookAfterStatusUpdate = [[UIAlertView alloc]initWithTitle:theTitle message:theMsg delegate:self cancelButtonTitle:nil otherButtonTitles: okBtnTitle, otherBtnTitle, nil];
    
    [alertViewFacebookAfterStatusUpdate show];
    
}
//////////////////////////////////////////////////
#pragma mark - UIAlert view - single input

-(void) setUIAlertViewShowInputWithTitle:(NSString*)title withMsg:(NSString*)msg withDefaultInputText:(NSString*)defaultInputText withOKBtnTitle:(NSString*)okBtnTitle  {
    
    alertViewInput = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:okBtnTitle, nil];
    
    [alertViewInput setAlertViewStyle:UIAlertViewStylePlainTextInput];
    
    [alertViewInput textFieldAtIndex:0].text = defaultInputText;
    
    [alertViewInput show];
    
    UITextRange *tr = [[alertViewInput textFieldAtIndex:0]textRangeFromPosition:[alertViewInput textFieldAtIndex:0].beginningOfDocument  toPosition:[alertViewInput textFieldAtIndex:0].endOfDocument];
    
    [alertViewInput textFieldAtIndex:0].selectedTextRange = tr;
    
    [UIPasteboard generalPasteboard].string = [alertViewInput textFieldAtIndex:0].text;
    
}
/////////////////////////////////////////////////////////
#pragma mark - Delegate callbacks

#pragma mark - email delegate
-(void) mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*) error {
    
    NSString *title;
    NSString *msg;
    
    if (error) {
        title = @"Oops";
        msg = @"There's a bit of a problem in sending your email. Please check your device to make sure you're able to send email, then try again.";
    }
    
    switch(result) {
        case MFMailComposeResultFailed: {
            title = @"Oops";
            msg = @"There's a bit of a problem in sending your email. Please check your device to make sure you're able to send emails, then try again.";
            
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Device Email Error 1 - check your device to make sure you're able to send emails, then try again" withLabel:[NSString stringWithFormat:@"Email Error desc: %@", error.description] withValue:nil];
            break;
        }//f
            
        case MFMailComposeResultCancelled: {
            title = @"Email Not Sent";
            
            switch(self.pSocialConnectionDetail.pShareType) { //share type
                case enumShareTypeAppRecommendation: {
                    msg = @"You've chosen to cancel this email. Pls remember to hook your friends up later. They're waitin' on you :)";
                    //@"You've chosen to cancel this email. Pls remember to share with your friends later. The more, the merrier :) \n \n We appreciate your support. Thanks!";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - Email Cancelled" withLabel:[NSString stringWithFormat:@"Email Cancelled - %@", self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl] withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                case enumShareTypeFave: {
                    msg = @"You've chosen to cancel this email. Pls remember to hook your friends up later. They're waitin' on you :)";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - Email Cancelled" withLabel:[NSString stringWithFormat:@"Email Cancelled - %@", self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl] withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                    
                case enumShareTypeCommunication: {
                    msg = @"You've chosen to cancel this email for now. Pls remember to email us at anytime. We love to hear from you :)";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - Email Cancelled" withLabel:@"Email Cancelled" withValue:nil];
                    break;
                }
                default: {
                    msg = @"You've chosen to cancel this email. Pls remember to hook your friends up later. They're waitin' on you :)";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - Email Cancelled" withLabel:@"Email Cancelled" withValue:nil];
                    break;
                }
            }//share type
        
            break;
        }
            
        case MFMailComposeResultSent: {
            title = @"Email Sent!";
            
            switch(self.pSocialConnectionDetail.pShareType) {//sharetype
                case enumShareTypeAppRecommendation: {
                    msg = @"Sweet! We really appreciate your shout-out. \n Thanks again. Peace.";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"App Recommendation - Email Sent!" withLabel:[NSString stringWithFormat:@"Email Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl] withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    
                    //ga real - email social sent
                    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - App Recommendation - Email Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                case enumShareTypeFave: {
                    msg = @"Sweet! Keep up the good work, champion. Peace.";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Fave Option - Email Sent!" withLabel:[NSString stringWithFormat:@"Email Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl] withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    
                    
                    //ga real - email social sent
                    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection -Fave Option - Email Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                default: {
                    msg = @"Sweet! Keep up the good work, champion. Peace.";
                    
                    //ga
                   [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - Email Sent!" withLabel:@"Email Sent!" withValue:nil];
                    
                    //ga real - email Communication sent
                   [PimpFitDataModel setGASendView:@"SocialConnection - Communication Option - Email Sent!"];
                    
                    break;
                }
            }//sharetype
            
           
           
            break;
        }
            
        case MFMailComposeResultSaved: {
            title = @"Message Saved.";
            
            switch(self.pSocialConnectionDetail.pShareType) { //share type
                case enumShareTypeAppRecommendation: {
                    msg = @"You've chosen to save your draft for now. Pls remember to hook your friends up later. They're waitin' on you :)";
                    //@"You've chosen to save your draft for now. Pls remember to share our app with your friends later. The more, the merrier :) \n \n We appreciate your support. Thanks!";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - Email Draft Saved" withLabel:[NSString stringWithFormat:@"Email Draft Saved - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl] withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                case enumShareTypeFave: {
                    msg = @"You've chosen to save your draft for now. Pls remember to hook your friends up later. They're waitin' on you :)";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Option - Email Draft Saved" withLabel:[NSString stringWithFormat:@"Email Draft Saved - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl] withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                 
                case enumShareTypeCommunication: {
                    msg = @"You've chosen to save your draft for now. Pls remember to email us at anytime. We love to hear from you :)";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - Email Cancelled" withLabel:@"Email Cancelled" withValue:nil];
                    break;
                }
                    
                default: {
                     msg = @"You've chosen to save your draft for now. Pls remember to hook your friends up later. They're waitin' on you :)";
                    
                    //@"You've chosen to save your email draft for now. Pls remember to send it later :)";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - Email Draft Saved" withLabel:@"Email Draft Saved" withValue:nil];
                    
                    break;
                }
            }//share type
        
            break;
        }
            
        default: {
            title = @"Oops";
            msg = @"There's a bit of a problem in sending your email. Please check your device to make sure you're able to send email, then try again.";
            
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Device Email Error 2 - check your device to make sure you're able to send email, then try again" withLabel:[NSString stringWithFormat:@"Email Error - Not Sent - Other Reason: %@", error.description] withValue:nil];
            
            break;
        }//default
    } //s
    
    //process email
    //close the email interface
    [controller dismissViewControllerAnimated:YES completion:^{
        //notify status
        //if(self.pSocialConnectionDetail.pShareType != enumShareTypeAppRecommendation) {
            [self setUIAlertViewShowWithTitle:title withMsg:msg withCancelBtnTitle:@"Okey Dokey" withOtherBtnTitle:nil];
            
       // }
        
    }];
    
    
}
//////////////////////////////////////////////////////////
#pragma mark - sms delegate

-(void) messageComposeViewController:(MFMessageComposeViewController*)controller didFinishWithResult:(MessageComposeResult)result {
    
    NSString *title;
    NSString *msg;
    
    switch (result) {
        case MessageComposeResultCancelled: {
            title = @"Text Not Sent";
            
            switch(self.pSocialConnectionDetail.pShareType) { //share type
                case enumShareTypeAppRecommendation: {
                   msg = @"You've chosen to cancel this text. Pls remember to hook your friends up later. They're waitin' on you :)";
                    
                    //@"You've chosen to cancel this text. Pls remember to share with your friends later. The more, the merrier :) \n \n We appreciate your support. Thanks!";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"App Recommendation - SMS Cancelled" withLabel:[NSString stringWithFormat:@"SMS Cancelled - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl] withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                case enumShareTypeFave: {
                    msg = @"You've chosen to cancel this text. Pls remember to hook your friends up later. They're waitin' on you :)";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"App Recommendation - SMS Cancelled" withLabel:[NSString stringWithFormat:@"SMS Cancelled - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl] withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                default: {
                      msg = @"You've chosen to cancel this text for now. Pls remember to text us at anytime. We love to hear from you :)";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication Option - SMS Cancelled" withLabel:@"SMS Cancelled" withValue:nil];
                    
                    break;
                }
            }//share type
            
            break;
        }//c
            
        case MessageComposeResultFailed: {
            title = @"Oops";
            msg = @"There's a bit of a problem in sending your text. Please check your device to make sure you're able to send SMS/Text, then try again.";
            
            //ga
        [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Device SMS Error 1 - check your device to make sure you're able to send SMS/Text, then try again" withLabel:[NSString stringWithFormat:@"SMS Error - %@", @"(unknown)"] withValue:nil];
            
            break;
        }
        case MessageComposeResultSent:{
            title = @"Text Sent";
            
            switch(self.pSocialConnectionDetail.pShareType) {//sharetype
                case enumShareTypeAppRecommendation: {
            msg = @"Sweet! We really appreciate your shout-out. \n Thanks again. Peace.";
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"App Recommendation - SMS Sent!" withLabel:[NSString stringWithFormat:@"SMS Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    
                    //ga real - sms
                    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - App Recommendation - SMS Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                case enumShareTypeFave: {
                    msg = @"Sweet! Keep up the good work, champion. Peace.";
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Fave - SMS Sent!" withLabel:[NSString stringWithFormat:@"SMS Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl ]withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    
                    //ga real - sms
                    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Fave - SMS Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
                     //pSelectedSocialConnectionFeedbackItem
                    break;
                }
                default : {
                    msg = @"Sweet! Keep up the good work, champion. Peace.";
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Communication - SMS Sent!" withLabel:@"SMS Sent!" withValue:nil];
                    
                    //ga real - sms
                    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Communication - SMS Sent! - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
                    //pSelectedSocialConnectionFeedbackItem
                    break;
                }
            }//sharetype
            
           
           
            break;
        }
            
        default: {
            title = @"Oops";
            msg = @"There's a bit of a problem in sending your text. Please check your device to make sure you're able to send SMS/Text, then try again.";
            
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Device SMS Error 2 - check your device to make sure you're able to send SMS/Text, then try again" withLabel:[NSString stringWithFormat:@"SMS Error - %@", @"(other-unknown)"] withValue:nil];
            
            break;
        }//default
    }//s
    
    //close sms interface
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    //show result msg
    [self setUIAlertViewShowWithTitle:title withMsg:msg withCancelBtnTitle:@"Okey Dokey"withOtherBtnTitle:nil ];
}
////////////////////////////////////////////////////////////////
#pragma mark - uialertview delegate - social share options

-(void) alertView:(UIAlertView*)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    //flag off active session immediately. Otherwise, sms or other invalid device setup causes view lock
    //- TODO:- needs a refactor. Delegate or block needed
     self.pIsSocialConnectionInSession = NO;
    
    if (alertView == alertViewShowOnly) {
        switch (selectedSocialConnectionType) {//selectedSocialConnectionType
            case enumSocialConnectionTypeSMS:
            case enumSocialConnectionTypeEmail: {
                //for communication share type, send out completion delegate
                switch(self.pSocialConnectionDetail.pShareType) {
                    case enumShareTypeCommunication:
                    //case enumShareTypeAppRecommendation:
                    {
                        [[self delegate]socialConnection:(SocialConnection*)self didCompleteCommunication:YES];
                        
                        //flag off active session - TODO:- needs refactor
                       // self.pIsSocialConnectionInSession = NO;
                        
                        break;
                    } //shareType-communication
                }//s-shareType
                
                break; //b-sms/email
            
            }//c -sms/email
            
            
            case enumSocialConnectionTypeTwitter: {//c-twitter
                     switch (currentSocialConnectionTypeStatus) {//currentSocialConnectionTypeStatus
                         case enumSocialConnectionTypeStatusTwitterTWTweetSDKCancelled: {
                             break;
                         }
                         case enumSocialConnectionTypeStatusTwitterTWTweetSDKDone: {
                             break;
                         }
                     }//s-currentSocialConnectionTypeStatus
                     
                     break; //b-twitter
                 }//c-twitter
                
                case enumSocialConnectionTypeFacebook: {
                    //no facebook - web interface fallback option
                    switch(currentSocialConnectionTypeStatus) {
                        case enumSocialConnectionTypeStatusFacebookNoDeviceSetup: {
                            [self setFacebookShowScreenForFallbackOptionWebInterface];
                            return;
                            break;
                        }//c-fb-web
                    }//s
                    //no facebook - web interface fallback option

                    break;//b-fb
                }//c-fb
        }//s - selectedSocialConnectionType
        
        //after user acknowledged ok showOnly alert, reload social options for further sharing
        //check whether to keep social options open
        switch(buttonIndex) {
            case 0: {
                (  self.pSocialConnectionDetail.pShouldKeepSocialOptionsOpenAfterFinishedSharing) ? [self setShowSocialNetworkAndCommunicationOptionsForViewController:self.self.pUIActionSheetHostViewController withSocialConnectionDetail:self.pSocialConnectionDetail]: nil;
               
                break;
            }
        }
       
    }//showonly
    
    
    //alertViewInput
    if(alertView == alertViewInput) {
        switch (selectedSocialConnectionType) {//selectedSocialConnectionType
            case enumSocialConnectionTypeTwitter: {//c-twitter
                switch (currentSocialConnectionTypeStatus) {//currentSocialConnectionTypeStatus
                    case enumSocialConnectionTypeStatusTwitterFallbackWebInterface:{
                        // web interface fallback option
                        //send to twitter login after ok - open in web safari
                        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://twitter.com"]];
                        break;
                        
                    }//c-twitter-fallback
                
                }//s-currentSocialConnectionTypeStatus
                
                break; //b-twitter
            }//c-twitter
                
            case enumSocialConnectionTypeFacebook: {
                switch (currentSocialConnectionTypeStatus) {//
                    case enumSocialConnectionTypeStatusFacebookFallbackWebInterface: {
                        //send to facebook login section after ok
                        //open in web safari
                        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://facebook.com"]];
                        break;
                        
                    }//c-fb-fallback
                }//s-currentSocialConnectionTypeStatus - fb
                break;//b-fb
            }//c-fb
        }//s - selectedSocialConnectionType
        
        //after user acknowledged ok inputAlert, screen switches to uiwebview, app reloads social options in bg. When app's active again, social options show for further sharing
        switch(buttonIndex) {
            case 0: {
                (self.pSocialConnectionDetail. pShouldKeepSocialOptionsOpenAfterFinishedSharing) ? [self setShowSocialNetworkAndCommunicationOptionsForViewController:self.pUIActionSheetHostViewController  withSocialConnectionDetail:self.pSocialConnectionDetail]: nil;
                //self.pSocialConnectionDetail.pHostViewController
                //
                break;
            }
        }

     }//alertViewInput
    
    
    //no twitter - show web fallback option
    if (alertView == alertViewNoTwitter) {
        [self setTwitterShowScreenForFallbackOptionWebInterface];
    }
    //no twitter - show web fallback option
    
    
    //facebook after status update, show ok & fb like option
    if (alertView == alertViewFacebookAfterStatusUpdate) {
        switch(buttonIndex) {
            case 0: {
                //selected ok
                //ga
                [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Tap Ok after Facebook status update" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
                //pSelectedSocialConnectionFeedbackItem
                
                //reprompt social options
            (self.pSocialConnectionDetail.pShouldKeepSocialOptionsOpenAfterFinishedSharing) ? [self setShowSocialNetworkAndCommunicationOptionsForViewController:self.pSocialConnectionDetail.pHostViewController withSocialConnectionDetail:self.pSocialConnectionDetail] :nil;
                break;
            } //c- fb ok
 
            case 1: {
                //selected fb like
                
                //ga
                [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Tap to Like on Facebook after status update" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
                //pSelectedSocialConnectionFeedbackItem
                
                //show fb like option
                [self setFacebookShowLikeView];
                
                // ga real fb like
            [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Tap to Like on Facebook after status update - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
                //pSelectedSocialConnectionFeedbackItem
                
                break;
            }//c-fb like

                                
        }
    }
    //facebook after status update
    
    
    //test context cue
   // [PimpFitUIModel setAnimateWithEffectWaterForViewController:self.pSocialConnectionDetail.pHostViewController withView:self.pSocialConnectionDetail.pHostViewController.view withDuration:-1];
    
}//end

/////////////////////////////////////////////////////////
#pragma mark - actionsheet

/**separate social /communications options */
-(void) setActionSheetDoSocialResult:(NSInteger)buttonIndex {
    
    //selectedSocialConnection type
    selectedSocialConnectionType = buttonIndex;
    
    switch(buttonIndex) {
        case enumSocialConnectionTypeSMS: {
            switch(self.pSocialConnectionDetail.pShareType) { //s-SMS-shareType
                    
                case enumShareTypeCommunication: {
                    
                    [self setSMSShowGeneralCommunicationScreenForViewController:self.pUIActionSheetHostViewController withSubject:self.pSocialConnectionDetail.pSubject withRecipients:self.pRecipients withGeneralMessageBody:self.pSocialConnectionDetail.pMsgBodyGeneralSMS isIMessage:self.pIsIMessage];
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Tap SMS button for Communication" withLabel:@"" withValue:nil];
                    
                    //ga real
                    [PimpFitDataModel setGASendView:@"SocialConnection - Tap SMS button for Communication"];
                    
                    //ga
                    break;
                } //enumShareTypeCommunication
                    
                    
                default: {
                    [self setSMSShowSocialScreenForViewController:self.pUIActionSheetHostViewController withRecipients:nil withGeneralMessageBody:self.pSocialConnectionDetail.pMsgBodyGeneralSMS isIMessage:self.pIsIMessage withSMSShortURLForApp:self.pSocialConnectionDetail.pAppUrlForSMS withAttachmentData:self.pSocialConnectionDetail.pMsgAttachmentData ofMimeType:self.pSocialConnectionDetail.pMsgAttachmentDataMIMEType withAttachmentDataFileName:self.pSocialConnectionDetail.pMsgAttachmentDataFileName];
                    //hostVC = actionsheet host vc
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Tap SMS button for sharing feedback" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    
                    //ga real
                [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Social Options - %@ - %@",@"Tap SMS button for sharing feedback",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
                    //pSelectedSocialConnectionFeedbackItem
                    //ga
                    
                    break;
                }//default - Social
                    
                    
            }//s-SMS-shareType
            
            break;
        } //enumSocialConnectionTypeSMS
            
        case enumSocialConnectionTypeEmail: {
            
            switch(self.pSocialConnectionDetail.pShareType) {
                    
                case enumShareTypeCommunication: {
                    [self setEmailShowGeneralCommunicationScreenForViewController:self.pUIActionSheetHostViewController withRecipients:self.pRecipients withSubject:self.pSocialConnectionDetail.pSubject withGeneralMessageBody:self.pSocialConnectionDetail.pMsgBodyGeneralEmail isHTMLType:self.pSocialConnectionDetail.pIsHTMLBasedEmail withAttachmentData:nil ofMimeType:nil withFileName:nil];
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Tap Email button for Communication" withLabel:@"" withValue:nil];
                    
                    //ga real
                    [PimpFitDataModel setGASendView:@"SocialConnection - Tap Email button for Communication"];
                    
                    //ga
                    
                    break;
                }//c-enumShareTypeCommunication
                    
                default: { //email - social
                    //update: handles attachment data
                    [self setEmailShowSocialScreenForViewController:self.pUIActionSheetHostViewController withRecipients:nil withSubject:self.pSocialConnectionDetail.pSubject withGeneralMessageBody:self.pSocialConnectionDetail.pMsgBodyGeneralEmail isHTMLType:self.pSocialConnectionDetail.pIsHTMLBasedEmail withEmailShortURLForApp:self.pSocialConnectionDetail.pAppUrlForEmail withAttachmentData:self.pSocialConnectionDetail.pMsgAttachmentData ofMimeType:self.pSocialConnectionDetail.pMsgAttachmentDataMIMEType withFileName:self.pSocialConnectionDetail.pMsgAttachmentDataFileName];
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Tap Email button for sharing feedback" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
                    //pSelectedSocialConnectionFeedbackItem
                    
                    //ga real
                    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Tap Email button for sharing feedback - %@",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
                    //pSelectedSocialConnectionFeedbackItem
                    //ga
                    
                    
                    break;
                }//default-ShareType Social
                    
            }//s- email - ShareType
            
            break;
        } //enumSocialConnectionTypeEmail
            
        case enumSocialConnectionTypeTwitter: {
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Tap Twitter button for sharing feedback" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
            //pSelectedSocialConnectionFeedbackItem
            
            //ga real
    [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Social Options - %@ - %@",@"Tap Twitter button for sharing feedback",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
            //pSelectedSocialConnectionFeedbackItem
            //ga
            
            [self setTwitterShowScreenForFrameworkInterface];
            
            break;
        }
            
        case enumSocialConnectionTypeFacebook:{
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Tap Facebook button for sharing feedback" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
            //pSelectedSocialConnectionFeedbackItem
            
            //ga real
        [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Social Options - %@ - %@",@"Tap Facebook button for sharing feedback",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
            //pSelectedSocialConnectionFeedbackItem
            //ga
            
            [self setFacebookShowScreenForViewController];
            
            break;
        }
            
        default: {
            //back - exit out
            
            /*
             * flag on in showSocialNetwork for fav shareType.
             * flag off in alertView dismiss - since sheet is always up after each social thread
             */
              self.pIsSocialConnectionInSession = NO;
            
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Social Options - Tap Back button at sharing feedback" withLabel:self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl withValue:nil];
            //pSelectedSocialConnectionFeedbackItem
            
            //ga real
            [PimpFitDataModel setGASendView:[NSString stringWithFormat:@"SocialConnection - Social Options - %@ - %@",@"Tap Back button at sharing feedback",self.pSocialConnectionDetail.pMsgBodyTextToShowBeforeAppUrl]];
            //pSelectedSocialConnectionFeedbackItem
            break;
        }
    }//s

}

////////////////////////////////////////
-(void) setActionSheetDoCommunicationResult:(NSInteger)buttonIndex {

    //selectedSocialConnectionType = buttonIndex;
    
    switch(buttonIndex) {
        case 0: {
            selectedSocialConnectionType = enumSocialConnectionTypeSMS;
            break;
        }
            
        case 1: {
            selectedSocialConnectionType = enumSocialConnectionTypeEmail;
            break;
        }
            
        case 2: {
            selectedSocialConnectionType = enumSocialConnectionTypeNone;
            break;
        }
    }//s
    
    
    switch(selectedSocialConnectionType) {
        case enumSocialConnectionTypeSMS: {
            switch(self.pSocialConnectionDetail.pShareType) { //s-SMS-shareType
                    
                case enumShareTypeCommunication: {
                    
                    [self setSMSShowGeneralCommunicationScreenForViewController:self.pUIActionSheetHostViewController withSubject:self.pSocialConnectionDetail.pSubject withRecipients:self.pRecipients withGeneralMessageBody:self.pSocialConnectionDetail.pMsgBodyGeneralSMS isIMessage:self.pIsIMessage];
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Tap SMS button for Communication" withLabel:@"" withValue:nil];
                    
                    //ga real
                    [PimpFitDataModel setGASendView:@"SocialConnection - Tap SMS button for Communication"];
                    
                    //ga
                    break;
                } //enumShareTypeCommunication
                
                    
            }//s-SMS-shareType
            
            break;
        } //enumSocialConnectionTypeSMS
            
        case enumSocialConnectionTypeEmail: {
            
            switch(self.pSocialConnectionDetail.pShareType) {
                    
                case enumShareTypeCommunication: {
                    //hostVC = actionsheet host vc
                    [self setEmailShowGeneralCommunicationScreenForViewController:self.pUIActionSheetHostViewController withRecipients:self.pRecipients withSubject:self.pSocialConnectionDetail.pSubject withGeneralMessageBody:self.pSocialConnectionDetail.pMsgBodyGeneralEmail isHTMLType:self.pSocialConnectionDetail.pIsHTMLBasedEmail withAttachmentData:nil ofMimeType:nil withFileName:nil];
                    
                    //ga
                    [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Tap Email button for Communication" withLabel:@"" withValue:nil];
                    
                    //ga real
                [PimpFitDataModel setGASendView:@"SocialConnection - Tap Email button for Communication"];
                    
                    //ga
                    
                    break;
                }//c-enumShareTypeCommunication
                    
                    
            }//s- email - ShareType
            
            break;
        } //enumSocialConnectionTypeEmail
         
        default: {
            //exit out - nothing done
            
            [[self delegate]socialConnection:(SocialConnection*)self didCompleteCommunication:YES];
            
            //ga
            [PimpFitDataModel setGASendEventWithCategory:@"SocialConnection" withAction:@"Tap exit from Communication Option" withLabel:@"Communication Option" withValue:nil];
            
            //ga real
            [PimpFitDataModel setGASendView:@"SocialConnection - Tap exit - Communication Option"];
            
            break;
        }
    
    }//s

}
////////////////////////////////////////////////////
#pragma mark - uiactionsheet delegate - selected SocialConnection type

-(void) actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (actionSheet == uiasSocial) {
        [self setActionSheetDoSocialResult:buttonIndex];
    }
    
    else if (actionSheet == uiasCommunication) {
        [self setActionSheetDoCommunicationResult:buttonIndex];
    }
    
}
////////////////////////////////////////////////////
@end

