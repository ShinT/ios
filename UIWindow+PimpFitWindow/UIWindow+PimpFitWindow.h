//
//  UIWindow+PimpFitWindow.h
//
//  Created by Shin Tan on 12/18/13.
//  Copyright (c) 2013 PimpFit.com. All rights reserved.
//

/**
 Desc:
 * UIWindow category - motion detection bug workaround.
 */
#import <UIKit/UIKit.h>

@interface UIWindow (PimpFitWindow)

+(NSString*)kShakeEvent;

@end
