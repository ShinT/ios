//
//  UIWindow+PimpFitWindow.m
//
//  Created by Shin Tan on 12/18/13.
//  Copyright (c) 2013 PimpFit.com. All rights reserved.
//

#import "UIWindow+PimpFitWindow.h"

@implementation UIWindow (PimpFitWindow)

////////////////////////////////////////////////////
#pragma mark - const
static NSString *const scShakeEvent = @"scShakeEvent";

////////////////////////////////////////////////////
#pragma mark - public getter
+(NSString*) kShakeEvent {
    return scShakeEvent;
}
////////////////////////////////////////////////////
#pragma mark - public method 

/** override motionBegan to send note*/
-(void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent*)event {
    //post
    [[NSNotificationCenter defaultCenter]postNotificationName:scShakeEvent object:nil];
    
    //override
    [super motionBegan:motion withEvent:event];
    
}

@end
