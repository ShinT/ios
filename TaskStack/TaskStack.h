//
//  TaskStack.h
//
//  Created by Shin Tan on 12/23/13.
//  Copyright (c) 2013 PimpFit.com. All rights reserved.
//
/**
 * Desc:
 */

/**
 - TODO:
 */
#import <Foundation/Foundation.h>

////////////////////////////////////////////////
#pragma mark - public enum
enum {
    enumStackActionNone = 0,
    enumStackActionPush = 1,
    enumStackActionPop = 2
};typedef int StackAction;

////////////////////////////////////////////////////
#pragma mark - public const

////////////////////////////////////////////////////
#pragma mark - public getter

/////////////////////////////////////////////////
#pragma mark - protocol delegate

/////////////////////////////////////////////////
#pragma mark - interface
@interface TaskStack : NSObject

////////////////////////////////////////////////
#pragma mark - delegate property

////////////////////////////////////////////////////
#pragma mark - class property

//in

//out

////////////////////////////////////////////////////
#pragma mark - property of type enum:

////////////////////////////////////////////////////
#pragma mark - public const

////////////////////////////////////////////////////
#pragma mark - public getters

/** task collection
 */
 -(NSMutableArray*) gNsmaContainer;

-(NSMutableArray*) gNsmaStack;

///////////////////////////////////////////////////
#pragma mark - public setters
-(void)sNsmaContainer :(NSMutableArray*)theContainer;
////////////////////////////////////////////////////
#pragma mark - public static getters

////////////////////////////////////////////////////
#pragma mark - public static setters

/////////////////////////////////////////////////////////
#pragma mark - constructors

///////////////////////////////////////
#pragma mark - class/static method declarations

////////////////////////////////////////////////////
#pragma mark - public method declarations

/**
 * Load - (re)init stack contents
 */
-(void) load:(id)theTask;
///////////////////////////////

/**
 * Push, return
 * filo
 */
-(id) push;

//////////////////////////////////

/**
 * pop, return
 * filo
 */
-(id) pop;
////////////////////////////////////
/**
 * stack count
 */

-(int) getContainerCount;
-(int) getStackCount;
/////////////////////////////////////

/**
 * check validity
 */
-(BOOL) isContainerEmpty;
-(BOOL) isStackEmpty;
////////////////////////

/**
 * nav ui visibility
 * In stack setup process: set uibNavLeft.enabled = NO;
 * set in setShowMediaContentWithStackAction after push/pop process
 self.uibNavLeft.enabled = [[self gMediaContentsStack]shouldShowPopNav];
 self.uibNavRight.enabled = [[self gMediaContentsStack]shouldShowPushNav];
 */
-(BOOL)shouldShowPopNav;

////////////////////////////

/**
 * nav ui visibility
 * set in ibaction after push/pop process
 self.uibNavLeft.enabled = [[self gMediaContentsStack]shouldShowPopNav];
 self.uibNavRight.enabled = [[self gMediaContentsStack]shouldShowPushNav];
 */
-(BOOL)shouldShowPushNav;
//////////////////////////////

/**
 * destack all. For (re)init
 */
-(void) setInitStack;

//////////////////////////////////
/**
 test
 */
+(void) setTestLogStack:(NSMutableArray*)theStack;
////////////////////////////////

@end
