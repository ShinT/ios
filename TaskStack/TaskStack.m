//
//  TaskStack.m
//
//  Created by Shin Tan on 12/23/13.
//  Copyright (c) 2013 PimpFit.com. All rights reserved.
//

#import "TaskStack.h"
/**
 - TODO:
 */
////////////////////////////////////////////////////
#pragma mark - implementation

@implementation TaskStack
{
    NSMutableArray *_gNsmaContainer;
    NSMutableArray *_gNsmaStack;
    int pvHead;
    
}
////////////////////////////////////////////////////
#pragma mark - public getters

-(NSMutableArray*) gNsmaContainer {
    if (!_gNsmaContainer) {
        _gNsmaContainer = [[NSMutableArray alloc]init];
    }
    return _gNsmaContainer;
}
/////////////////////////////////////
-(NSMutableArray*) gNsmaStack {
    if (!_gNsmaStack) {
        _gNsmaStack = [[NSMutableArray alloc]init];
    }
    return _gNsmaStack;
}
///////////////////////////////////////////////////
#pragma mark - public setters

-(void)sNsmaContainer :(NSMutableArray*)theContainer {
    _gNsmaContainer = theContainer;
}
////////////////////////////////////////////////////
#pragma mark - delegate property synthesis

////////////////////////////////////////////////////
#pragma mark - class property synthesis

///////////////////////////////////////////////////
#pragma mark - private getters

///////////////////////////////////////////////////
#pragma mark - private setters

////////////////////////////////////////////////////
#pragma mark - private static const

///////////////////////////////////////////////////
#pragma mark - private static vars

////////////////////////////////////////////////////
#pragma mark - public static getters

///////////////////////////////////////////////////
#pragma mark - public static setters

///////////////////////////////////////////////////
#pragma mark - constructors

///1
- (id) init {
    if (self = [super init]) {
        // init stuff
        pvHead = -1;
        
    }
    return self;
}

//////////////////////////////////////////
#pragma mark - destructor
/*
 -(void) setDispose {
 //cleanup
 }
 */
////////////////////////////////////////////////////
#pragma mark - class/static method implementation

////////////////////////////////////////////////////
#pragma mark - public method implementation

/**
 * Load - (re)init stack contents
 */
-(void) load:(id)theTask {
    [[self gNsmaContainer]addObject:theTask];
}
///////////////////////////////////////////////////
/**
 * Push, return 
 * grab from container per pvHead index, push to stack, return the lifo
 */
-(id) push {
    pvHead++;
    id i;
    //dLog(@"container: %i",[self getContainerCount]);
    if (pvHead > [self getContainerCount]-1) {
        //dLog(@"push: ptr max. Curr:%i",pvHead);
        [PimpFitDataModel setTestLogForClass:@"TaskStack" withMethod:@"push" withKey:@"push: ptr max" withValue:[NSString stringWithFormat:@"Curr: %i",pvHead]];
        return nil;
    }
    else  {
      [[self gNsmaStack] addObject:[[self gNsmaContainer]objectAtIndex:pvHead]];
        
        i =  [[self gNsmaStack]objectAtIndex:[self getStackCount]-1];
        
    }
    return i;
}
/////////////////////////////////////////////////////

/**
 * pop from lifo
 */
-(id) pop {
    //update index
   pvHead--;
    //dLog(@"pop currentPushIndex: %i",svCurrentPtrIndex);
    
    //unload present stack before popping prev
    [self unloadAtIndex:[self getStackCount]-1]; //
    
    //dLog(@"pop: current stack count: %i",(int)[[self gNsmaStack]count]);
    
    //pop prev
    id i;
    if ([self getStackCount]>0 ) {
        i = [[self gNsmaStack] objectAtIndex:[self getStackCount]-1];//
    }
    else {
        i = nil;
       // dLog(@"pop: return nil. stack count: %i",(int)[self getStackCount]);
         [PimpFitDataModel setTestLogForClass:@"TaskStack" withMethod:@"pop" withKey:@"pop: return nil" withValue:[NSString stringWithFormat:@"stack count: %i",(int)[self getStackCount]]];
    }
    
    return i;
    
}
////////////////////////////////////////////////////////

/**
 * stack count
 */

-(int) getContainerCount {
    return (int)[[self gNsmaContainer]count];
}

///////////////////////////////////////////////////////
-(int) getStackCount {
    
    return (int)[[self gNsmaStack]count];
    
}

//////////////////////////////////////////////////////

/**
 * check validity
 */
-(BOOL) isContainerEmpty {
    return ([[self gNsmaContainer]count] >0) ? NO :YES;
}
/////////////////////////////////////////////////////
-(BOOL) isStackEmpty {
    return ([[self gNsmaStack]count] >0) ? NO :YES;
}

/////////////////////////////////////////////////////
/**
 * navig
 */

-(BOOL) shouldShowPopNav {
    if (pvHead<=0) {
        return NO;
    }
    return YES;
}
////////////////////////////////////////////////////
-(BOOL) shouldShowPushNav {
    if (pvHead >= self.getContainerCount-1) {
        return NO;
    }

    return YES;
}
///////////////////////////////////////////////////

/**
 * destack all. For (re)init
 */
-(void) setInitStack {
    [[self gNsmaContainer]removeAllObjects];
    [[self gNsmaStack]removeAllObjects];
    pvHead = -1;
}

//////////////////////////////////////////////////
/**
 test
 */
+(void) setTestLogStack:(NSMutableArray*)theStack {
    for (id i in theStack) {
        //dLog(@"id: %@",i);
        [PimpFitDataModel setTestLogForClass:@"TaskStack" withMethod:@"setTestLogStack" withKey:@"theStack" withValue:[NSString stringWithFormat:@"%@",i]];
    }
}


////////////////////////////////////////////////////
#pragma mark - private method implementation

/**
 * Unloads, returns
 */

-(void) unloadAtIndex:(NSUInteger)theIndex {
    [[self gNsmaStack]removeObjectAtIndex:theIndex];
}
/*
-(id) unloadForwardStack {
    id i = [[self gNsmaForwardStack]objectAtIndex:0];
    [[self gNsmaForwardStack] removeObjectAtIndex:0];
    return i;
}
///////////////////////////////
-(id) unloadReverseStack {
    id i = [[self gNsmaReverseStack] objectAtIndex:[self getReverseStackCount]-1];
    [[self gNsmaReverseStack]removeObjectAtIndex:[self getReverseStackCount]-1];
    
    return i;
}
 */
////////////////////////////////////////////////////
#pragma mark - delegate callback listeners

@end
