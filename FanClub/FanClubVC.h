//
//  FanClubVC.h
//
//  Created by Shin Tan on 1/7/14.
//  Copyright (c) 2014 PimpFit.com. All rights reserved.
//
/**
 * desc
 */

/**
 * TODO
 */
#import <UIKit/UIKit.h>
#import "FanClub.h"
////////////////////////////////////////////////
#pragma mark - public enum

/////////////////////////////////////////////////
#pragma mark - protocol delegate

/////////////////////////////////////////////////
#pragma mark - interface
@interface FanClubVC : UIViewController
<UITextFieldDelegate>
////////////////////////////////////////////////
#pragma mark - delegate property

////////////////////////////////////////////////////
#pragma mark - class property

//in
@property(strong,nonatomic) FanClub *pFanClub;

//out
////////////////////////////////////////////////////
#pragma mark - IBOutlet property
@property(weak,nonatomic) IBOutlet UILabel *uilTitle;
@property(weak,nonatomic) IBOutlet UILabel *uilMsg;
@property(weak,nonatomic) IBOutlet UITextField *uitfFirstName;
@property(weak,nonatomic) IBOutlet UITextField *uitfLastName;
@property(weak,nonatomic) IBOutlet UITextField *uitfEmail;
@property(weak,nonatomic) IBOutlet UIButton *uibJoinClub;

@property(weak,nonatomic) IBOutlet UIView *uivResult;
@property(weak,nonatomic) IBOutlet UILabel *uilResult;
@property(weak,nonatomic) IBOutlet UIButton *uibResult;
@property(weak,nonatomic) IBOutlet UIImageView *uiivResult;

/**error indicators*/
@property(weak,nonatomic) IBOutlet UIImageView *uiivIndicatorFirstName;

@property(weak,nonatomic) IBOutlet UIImageView *uiivIndicatorLastName;

@property(weak,nonatomic) IBOutlet UIImageView *uiivIndicatorEmail;

////////////////////////////////////////////////////
#pragma mark - IBAction method declarations
-(IBAction) uibJoinClub_TouchUpInside:(id)sender;

-(IBAction) uibResult_TouchUpInside:(id)sender;

-(IBAction) uibHome_TouchUpInside:(id)sender;
////////////////////////////////////////////////////
#pragma mark - constructor

////////////////////////////////////////////////////
#pragma mark - public method declarations
@end
