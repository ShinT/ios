//
//  FanClubVC.m
//
//  Created by Shin Tan on 1/7/14.
//  Copyright (c) 2014 PimpFit.com. All rights reserved.
//

#import "FanClubVC.h"
#import "PimpFitUIModel.h"

/**
 * TODO
 */
@interface FanClubVC ()

@end
////////////////////////////////////////////////////
#pragma mark - private enum

////////////////////////////////////////////////////
#pragma mark - implementation
@implementation FanClubVC {}
////////////////////////////////////////////////////
#pragma mark - static const
//ui
/** after action pos/size*/
static CGFloat scUIVResultAfterResultX = 10.0;
static CGFloat scUIVResultAfterResultY = 393.0;

////////////////////////////////////////////////////
#pragma mark - delegate property synthesis

////////////////////////////////////////////////////
#pragma mark - class property synthesis

////////////////////////////////////////////////////
#pragma mark - ui property synthesis

////////////////////////////////////////////////////
#pragma mark - getters

////////////////////////////////////////////////////
#pragma mark - public method implementations
////////////////////////////////////////////////////
#pragma mark - private method implementations

#pragma mark - validation
-(void) setDoValidation {
    BOOL validated1 = NO;
    BOOL validated2 = NO;
    BOOL validated3 = NO;
    
    validated1 = [PimpFitUIModel validateIsTextFieldWithLettersValidated:self.uitfFirstName withErrorImageView:self.uiivIndicatorFirstName withErrorImageNamed:@"info.png"];
    
    validated2 = [PimpFitUIModel validateIsTextFieldWithLettersValidated:self.uitfLastName withErrorImageView:self.uiivIndicatorLastName withErrorImageNamed:@"info.png"];
    
    validated3 = [PimpFitUIModel validateIsTextFieldWithEmailValidated:self.uitfEmail withErrorImageView:self.uiivIndicatorEmail withErrorImageNamed:@"info.png"];
    
    self.uibJoinClub.enabled = (validated1 && validated2 && validated3) ? YES : NO;
}

//////////////////////////////////////////////////////////
#pragma mark - Join Club

#pragma mark - email list signup

/**
 * Sign up.
 * Caller: handle completion block.
 */
-(void) setDoEmailListSignUp {
    //lock tf, ub
    self.uitfFirstName.enabled = NO;
    self.uitfLastName.enabled = NO;
    self.uitfEmail.enabled = NO;
    self.uibJoinClub.enabled = NO;
    
    __unsafe_unretained typeof(self)weakSelf = self;
    
    //activity on
    [PimpFitUIModel setActivityIndicatorShowForViewController:self withActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge withCenter:self.view.center withViewControllerViewAlpha:0.6 withActivityIndicatorViewBackgroundColor:[UIColor lightGrayColor] withActivityIndicatorViewBackgroundAlpha:0.8 withActivityIndicatorColor:[UIColor blackColor] withText:@"Hooking you up. Pls wait.." withTextColor:[UIColor blackColor]];
    
    [self.pFanClub setSignUpForEmailListWithEmail:self.uitfEmail.text withFirstName:self.uitfFirstName.text withLastName:self.uitfLastName.text withFanClubResultCompletion:^(FanClubResult fanClubResult){
    
        //props
        weakSelf.pFanClub.pFanClubResult = fanClubResult;
        
        //activity off
        [PimpFitUIModel setActivityIndicatorUnshowForViewController:weakSelf withBackgroundViewAlpha:1.0 afterDelay:1.0];
        
        [weakSelf setShowEmailListSignUpWithResult:fanClubResult];
        
    }];
}
///////////////////////////////////////

/**
 * sign up email list results.
 */
-(void) setShowEmailListSignUpWithResult:(FanClubResult)theResult {
   
    //activity off
   //[PimpFitUIModel setActivityIndicatorUnshowForViewController:self withBackgroundViewAlpha:1.0 afterDelay:1.0];
    
    [PimpFitUIModel setAnimateUIControl:(UIControl*)self.uivResult withDuration:[PimpFitUIModel UIAnimationPropertyDurationDefaultShorter] - 0.5 withDelay:[PimpFitUIModel UIAnimationPropertyDelayLoadView] withAnimationOption:UIViewAnimationOptionCurveEaseIn withAnimations:^{
        
        switch(theResult) {
                
            case enumFanClubResultConnectionFail:{
                self.uilResult.text = @"Pls check your internet connection & try again.."; //override this for now self.pFanClub.pResultFailText;
                
                self.uiivResult.image = (self.pFanClub.pResultFailImageNamed.length>0) ? [UIImage imageNamed:self.pFanClub.pResultFailImageNamed] : [UIImage imageNamed:@"error 2.png"];
                
                //reset cmd to try again
              
                 [self.uibResult setTitle:@"Try Again Now" forState:UIControlStateNormal];
                 [self.uibResult setTitle:@"Try Again Now" forState:UIControlStateHighlighted];
                
                //ga
                [PimpFitDataModel setGASendView:@"FanClubVC - Email List Sign Up - FanClubResultConnectionFail"];
                
                [PimpFitDataModel setGASendEventWithCategory:@"FanClubVC" withAction:@"Email List Sign Up" withLabel:@"FanClubResultConnectionFail" withValue:nil];
                
                 break;
            }//c-conn
                
            case enumFanClubResultSignUpEmailListFail: {
                self.uilResult.text = self.pFanClub.pResultFailText;
                
                self.uiivResult.image = (self.pFanClub.pResultFailImageNamed.length>0) ? [UIImage imageNamed:self.pFanClub.pResultFailImageNamed] : [UIImage imageNamed:@"error 2.png"];
            
                //cmd
                [self.uibResult setTitle:(self.pFanClub.pResultFailCommandText.length>0)? self.pFanClub.pResultFailCommandText : @"I will try again later" forState:UIControlStateNormal];
                
                [self.uibResult setTitle:(self.pFanClub.pResultFailText.length>0) ? self.pFanClub.pResultFailCommandText : @"I will try again later" forState:UIControlStateHighlighted];
                
                //ga
                [PimpFitDataModel setGASendView:@"FanClubVC - Email List Sign Up - FanClubResultSignUpEmailListFail"];
                
                [PimpFitDataModel setGASendEventWithCategory:@"FanClubVC" withAction:@"Email List Sign Up" withLabel:@"FanClubResultSignUpEmailListFail" withValue:nil];
                break;
            }//c-f
                
            case enumFanClubResultSignUpEmailListSuccess: {
                self.uilResult.text = [NSString stringWithFormat:@"%@, %@.\n%@",self.pFanClub.pResultSuccessText1,self.uitfFirstName.text, self.pFanClub.pResultSuccessText2];
                
                self.uiivResult.image = (self.pFanClub.pResultSuccessImageNamed.length>0) ? [UIImage imageNamed:self.pFanClub.pResultSuccessImageNamed] : [UIImage imageNamed:@"tick 2.png"];
                
                [self.uibResult setTitle:(self.pFanClub.pResultSuccessCommandText.length>0)? self.pFanClub.pResultSuccessCommandText : @"Done" forState:UIControlStateNormal];
                [self.uibResult setTitle:(self.pFanClub.pResultSuccessCommandText.length>0) ? self.pFanClub.pResultSuccessCommandText : @"Done" forState:UIControlStateHighlighted];
               
                //ga
                [PimpFitDataModel setGASendView:@"FanClubVC - Email List Sign Up - FanClubResultSignUpEmailListSuccess!"];
                
                [PimpFitDataModel setGASendEventWithCategory:@"FanClubVC" withAction:@"Email List Sign Up" withLabel:@"FanClubResultSignUpEmailListSuccess!" withValue:nil];
                
                break;
            }//c-s

        }//s
        
        //push result view
        CGRect rect = self.uivResult.frame;
        rect.origin.x = scUIVResultAfterResultX;
        rect.origin.y = scUIVResultAfterResultY;
        self.uivResult.frame = rect;
        
        self.uivResult.alpha = 1.0;
        
        self.uibResult.alpha = 1.0;

        
    } withCompletion:^(BOOL finished) {
        
        if (theResult == enumFanClubResultSignUpEmailListSuccess) {
            self.uiivIndicatorFirstName.image = [UIImage imageNamed:@"award.png"];
            self.uiivIndicatorFirstName.hidden = NO;

        }
    }];
    
    // self.uilResult.text = (success) ? [NSString stringWithFormat:@"%@, %@.\n%@",self.pFanClub.pResultSuccessText1,self.uitfFirstName.text, self.pFanClub.pResultSuccessText2] : self.pFanClub.pResultFailText;
}

//////////////////////////////////////////////////////////
#pragma mark - View Tasks

-(void) setShowAllViews {
    
    [PimpFitUIModel setAnimateUIControl:(UIControl*)self.uilTitle withDuration:[PimpFitUIModel UIAnimationPropertyDurationLoadAlertTextShortest] withDelay:0.0 withAnimationOption:UIViewAnimationOptionCurveEaseIn withAnimations:^{
        
        self.uilTitle.alpha = 1.0;
        
        
    } withCompletion:^(BOOL finished) {
        
        [PimpFitUIModel setAnimateUIControl:(UIControl*)self.uilMsg withDuration:[PimpFitUIModel UIAnimationPropertyDurationLoadMsgTextShorter]withDelay:0.0 withAnimationOption:UIViewAnimationOptionCurveEaseIn withAnimations:^{
            
            self.uilMsg.alpha = 1.0;
            
            [PimpFitUIModel setAnimateUIControl:(UIControl*)self.uitfFirstName withDuration:[PimpFitUIModel UIAnimationPropertyDurationDefaultShorter] withDelay:0.0 withAnimationOption:UIViewAnimationOptionCurveEaseIn withAnimations:^{
                
                self.uitfFirstName.alpha = 1.0;
                self.uitfLastName.alpha = 1.0;
                self.uitfEmail.alpha = 1.0;
                
                [PimpFitUIModel setAnimateUIControl:(UIControl*)self.uibJoinClub withAnimationOption:UIViewAnimationOptionCurveEaseIn withDelay:0.0 withAlpha:1.0];
                
                
            } withCompletion:^(BOOL finished){
                
            }];//completion - uitf
            
        } withCompletion:^(BOOL finished){
            
        }];//completion - uilMsg
        
    }]; //completion - uilTitle
}

//////////////////////////////////////////////////////////
#pragma mark - delegate callbacks

////////////////////////////////////////////////////////
#pragma mark - selector

////////////////////////////////////////////////////////
#pragma mark - IBActions

-(IBAction) uibJoinClub_TouchUpInside:(id)sender {
    
    //ga
    [PimpFitDataModel setGASendView:@"FanClubVC - Tap Join Club"];
    
    [PimpFitDataModel setGASendEventWithCategory:@"FanClubVC" withAction:@"Tap Join Club" withLabel:@"Count" withValue:nil];
    
    [self setDoEmailListSignUp];
    //[self setShowEmailSignUpResultSuccess:YES];
   
  
}

/////////////////////////////////////////////
-(IBAction) uibResult_TouchUpInside:(id)sender {
    
    [self setDoAfterEmailListSignUpTasks];
}

/////////////////////////////////////////////
#pragma mark - after email list sign up tasks

/**
 * options to handle:
  - retry previous connection fail - no dismiss
  - confirm retry later - dismiss ok
 */

-(void) setDoAfterEmailListSignUpTasks {
   
    //strange bug: zero out result view 1st. Otherwise, ibaction caused view to move back to original coord before dismissing during animate?
    //self.uivResult.hidden = YES;
     self.uivResult.alpha = 0.0;
    
    switch(self.pFanClub.pFanClubResult) {
        case enumFanClubResultConnectionFail : {
            //flag on result view again before retry, otherwise, wont show
            //self.uivResult.alpha = 0.0;
            //self.uivResult.hidden = NO;
            
            //retry sign up
            [self setDoEmailListSignUp];
            
            //ga
            [PimpFitDataModel setGASendView:@"FanClubVC - Email List Sign Up - Retry Again"];
            
            [PimpFitDataModel setGASendEventWithCategory:@"FanClubVC" withAction:@"Email List Sign Up" withLabel:@"Retry Again" withValue:nil];

            break;
        }//c-conn
            
        case enumFanClubResultSignUpEmailListFail: {
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
            break;
        }
            
        case enumFanClubResultSignUpEmailListSuccess: {
            
            //ga
            [PimpFitDataModel setGASendView:@"FanClubVC - Email List Sign Up - FanClubResultSignUpEmailListSuccess - Done"];
            
            [PimpFitDataModel setGASendEventWithCategory:@"FanClubVC" withAction:@"Email List Sign Up" withLabel:@"FanClubResultSignUpEmailListSuccess - Done" withValue:nil];

            
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
            break;
        }
            
        default: {
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
            break;
        }//d
    }//s
    
}
/////////////////////////////////////////////

-(IBAction) uibHome_TouchUpInside:(id)sender {
    //strange bug: if not hiding view, ibaction caused view to move back to original coord before dismissing?
    self.uivResult.hidden = YES;
    
    //ga
    [PimpFitDataModel setGASendView:@"FanClubVC - Tap Home"];
    
    [PimpFitDataModel setGASendEventWithCategory:@"FanClubVC" withAction:@"Tap Home" withLabel:@"Count" withValue:nil];

    
    [self dismissViewControllerAnimated:YES completion:nil];
}
/////////////////////////////////////////////////////////
#pragma mark - ui delegate methods

#pragma mark - text fields
-(BOOL) textFieldShouldReturn:(UITextField*)textField {
    [self setDoValidation];
    return [textField resignFirstResponder];
    
}
/////////////////////////////////////////////////////////
#pragma mark - segue

/////////////////////////////////////////////////////////
#pragma mark - init

////////////////////////////////////////////////////////
#pragma mark - UI Colors /images
-(void) setInitUIColorsAndImages {
    
    //duplicate uiv bg onInit as view bg
    self.uivResult.backgroundColor = gcColorWhiteSmoke;
    
    [self.uibJoinClub setTitleColor:cColorGeneralUIControlFore forState:UIControlStateNormal];

    [self.uibJoinClub setTitleColor:cColorGeneralUIControlFore forState:UIControlStateHighlighted];
    
    [self.uibJoinClub setTitleColor:cColorGeneralUIControlForeInactive forState:UIControlStateDisabled];
    
    [self.uibResult setTitleColor:cColorGeneralUIControlFore forState:UIControlStateNormal];
    
    [self.uibResult setTitleColor:cColorGeneralUIControlForeInactive forState:UIControlStateDisabled];
    
     [self.uibResult setTitleColor:cColorGeneralUIControlFore forState:UIControlStateHighlighted];

}
/////////////////////////////////////////////////////////
-(void) setInit {
    
    [self setInitUIColorsAndImages];
    
    //init state
    self.uilTitle.alpha = 0.0;
    self.uilMsg.alpha = 0.0;
    self.uibJoinClub.alpha = 0.0;
    self.uibResult.alpha = 0.0;
    self.uivResult.alpha = 0.0;
    self.uitfFirstName.alpha = 0.0;
    self.uitfLastName.alpha = 0.0;
    self.uitfEmail.alpha = 0.0;
    
    //zero out cmd txt - else animate block causes it to flickr default txt before showing correct txt
    [self.uibResult setTitle:@"" forState:UIControlStateNormal];
    [self.uibResult setTitle:@"" forState:UIControlStateHighlighted];
    
    //tf
    self.uitfFirstName.text = @"";
    self.uitfLastName.text = @"";
    self.uitfEmail.text = @"";
    self.uitfFirstName.delegate = self;
    self.uitfLastName.delegate = self;
    self.uitfEmail.delegate = self;
    
    //error indicators
    self.uiivIndicatorFirstName.hidden = YES;
    self.uiivIndicatorLastName.hidden = YES;
    self.uiivIndicatorEmail.hidden = YES;
    
    //tf, cmd
    
    self.uibJoinClub.enabled = NO;
    
    //run time props
    if (self.pFanClub) {
        self.uilTitle.text = (self.pFanClub.pTitle.length>0)? self.pFanClub.pTitle :self.uilTitle.text;
        
        self.uilMsg.text = (self.pFanClub.pMsg.length>0) ? self.pFanClub.pMsg : self.uilMsg.text;
    }
}
////////////////////////////////////////////////////////
#pragma mark - constructor

/////////////////////////////////////////////////////////
#pragma mark - destructor

/**
 note: viewDidUnload never gets called when storyboarding via segue
 solution: run this in prepareForSegue to make sure custom objs (i.e iVars) properly disposed after tapping '+'
 */
- (void) setDispose {
    //custom objs = nil (ivars etc)
}

/////////////////////////////////////////////////////////
#pragma mark - Testing

/////////////////////////////////////////////////////////
#pragma mark - xcode view default

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib
    [self setInit];
    
}
/////////////////////////////////////////////////////////

-(void) viewWillAppear:(BOOL)animated {

}

/////////////////////////////////////////////////////////
-(void) viewDidAppear:(BOOL)animated {
    //activate
    [self setShowAllViews];

    //ga
    [PimpFitDataModel setGASendView:@"FanClubVC"];

}
/////////////////////////////////////////////////////////
-(void) viewDidDisappear:(BOOL)animated {

}
/////////////////////////////////////////////////////////
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}
/////////////////////////////////////////////////////////


@end
