//
//  FanClub.m
//
//  Created by Shin Tan on 1/8/14.
//  Copyright (c) 2014 PimpFit.com. All rights reserved.
//

#import "FanClub.h"
#import "ChimpKit.h"

/**
 - TODO:
 */

////////////////////////////////////////////////////
#pragma mark - implementation
@implementation FanClub {}
////////////////////////////////////////////////////
#pragma mark - public getters

///////////////////////////////////////////////////
#pragma mark - public setters

////////////////////////////////////////////////////
#pragma mark - delegate property synthesis

////////////////////////////////////////////////////
#pragma mark - class property synthesis

///////////////////////////////////////////////////
#pragma mark - private getters

///////////////////////////////////////////////////
#pragma mark - private setters

////////////////////////////////////////////////////
#pragma mark - private static const

///////////////////////////////////////////////////
#pragma mark - private static vars

////////////////////////////////////////////////////
#pragma mark - public static getters

///////////////////////////////////////////////////
#pragma mark - public static setters

///////////////////////////////////////////////////
#pragma mark - constructors

+(id) fanClubWithTitle:(NSString*)theTitle withMsg:(NSString*)theMsg withJoinClubCommandText:(NSString*)theJoinClubCommandText withResultSuccessCommandText:(NSString*)theResultSuccessCommandText withResultFailCommandText:(NSString*)theResultFailCommandText withResultFailText:(NSString*)theResultFailText withResultSuccessText1:(NSString*)theResultSuccessText1 withResultSuccessText2:(NSString*)theResultSuccessText2 withResultFailImageNamed:(NSString*)theResultFailImageNamed withResultSuccessImageNamed:(NSString*)theResultSuccessImageNamed withChimpKitApiKey:(NSString*)theApiKey withChimpKitListId:(NSString*)theListId {
    
   return [[FanClub alloc]initFanClubWithTitle:theTitle withMsg:theMsg withJoinClubCommandText:theJoinClubCommandText withResultSuccessCommandText:(NSString*)theResultSuccessCommandText withResultFailCommandText:(NSString*)theResultFailCommandText withResultFailText:(NSString*)theResultFailText withResultSuccessText1:(NSString*)theResultSuccessText1 withResultSuccessText2:(NSString*)theResultSuccessText2 withResultFailImageNamed:theResultFailImageNamed withResultSuccessImageNamed:theResultSuccessImageNamed withChimpKitApiKey:theApiKey withChimpKitListId:theListId];
}

/////////////////////////////////////////////////
-(id) initFanClubWithTitle:(NSString*)theTitle withMsg:(NSString*)theMsg withJoinClubCommandText:(NSString*)theJoinClubCommandText withResultSuccessCommandText:(NSString*)theResultSuccessCommandText withResultFailCommandText:(NSString*)theResultFailCommandText withResultFailText:(NSString*)theResultFailText withResultSuccessText1:(NSString*)theResultSuccessText1 withResultSuccessText2:(NSString*)theResultSuccessText2  withResultFailImageNamed:(NSString*)theResultFailImageNamed withResultSuccessImageNamed:(NSString*)theResultSuccessImageNamed withChimpKitApiKey:(NSString*)theApiKey withChimpKitListId:(NSString*)theListId{
    
    if (self= [super init]) {
        self.pTitle = theTitle;
        self.pMsg = theMsg;
        self.pJoinClubCommandText = theJoinClubCommandText;
        self.pResultSuccessCommandText = theResultSuccessCommandText;
        self.pResultFailCommandText = theResultFailCommandText;
        self.pResultFailText = theResultFailText;
        self.pResultSuccessText1 = theResultSuccessText1;
        self.pResultSuccessText2 = theResultSuccessText2;
        self.pResultFailImageNamed = theResultFailImageNamed;
        self.pResultSuccessImageNamed = theResultSuccessImageNamed;
        
        self.pChimpKitApiKey = theApiKey;
        self.pChimpKitListId = theListId;
        
        
    }
    return self;
}

//////////////////////////////////////////
#pragma mark - destructor
/*
 -(void) setDispose {
 //cleanup
 }
 */
////////////////////////////////////////////////////
#pragma mark - class/static method implementation

////////////////////////////////////////////////////
#pragma mark - public method implementation

/**
 * email list signup.
 * Caller: handle fanClubResultCompletion block.
 * currently supporting ck API 3.0
 */
-(void) setSignUpForEmailListWithEmail:(NSString*)theEmail withFirstName:(NSString*)theFirstName withLastName:(NSString*)theLastName  withFanClubResultCompletion:(void(^)(FanClubResult fanClubResultCompletion))completedWithFanClubResult {
    
    [[ChimpKit sharedKit] setApiKey:self.pChimpKitApiKey];//scChimpKitAPIKey
    
    NSDictionary *nsdListData = @{@"id":self.pChimpKitListId, @"email":@{@"email":theEmail, @"merge_vars" : @{@"FNAME" : theFirstName, @"LNAME" : theLastName}}};
    //scChimpKitListId
    
    //test - note: change callApiMethod to lists/testsubscribe  @"lists/subscribe"
    NSString *chimpKitApiMethod;
    switch (cChimpKitOn) {
        case YES: { //release
            chimpKitApiMethod = @"lists/subscribe";
            break;
        }//c-y-release
            
        case NO: {//debug
            chimpKitApiMethod = @"lists/testsubscribe";
            //returns enumFanClubResultSignUpEmailListFail
           // dLog(@"fn:%@",theFirstName);
             [PimpFitDataModel setTestLogForClass:@"FanClub" withMethod:@"setSignUpForEmailListWithEmail" withKey:@"fn" withValue:[NSString stringWithFormat:@"%@",theFirstName]];
            
           // dLog(@"ln:%@",theLastName);
            [PimpFitDataModel setTestLogForClass:@"FanClub" withMethod:@"setSignUpForEmailListWithEmail" withKey:@"ln" withValue:[NSString stringWithFormat:@"%@",theLastName]];
            
            //dLog(@"em:%@",theEmail);
              [PimpFitDataModel setTestLogForClass:@"FanClub" withMethod:@"setSignUpForEmailListWithEmail" withKey:@"em" withValue:[NSString stringWithFormat:@"%@",theEmail]];
            
            //additional tests - skips callApiMethod
            //completedWithFanClubResult(enumFanClubResultConnectionFail);
            //completedWithFanClubResult(enumFanClubResultSignUpEmailListFail);
            completedWithFanClubResult(enumFanClubResultSignUpEmailListSuccess);
           
            chimpKitApiMethod = nil;
            return; //out
            break;
        }//c-n-debug
    }//s
    
    [[ChimpKit sharedKit] callApiMethod:chimpKitApiMethod withParams:nsdListData andCompletionHandler:^(ChimpKitRequest *request, NSError *error) {
    
        //dLog(@"ck http status:%d",request.response.statusCode);
        [PimpFitDataModel setTestLogForClass:@"FanClub" withMethod:@"setSignUpForEmailListWithEmail" withKey:@"ck http status" withValue:[NSString stringWithFormat:@"%d",request.response.statusCode]];
        
       // dLog(@"ck response: %@", request.responseString);
        [PimpFitDataModel setTestLogForClass:@"FanClub" withMethod:@"setSignUpForEmailListWithEmail" withKey:@"ck response" withValue:[NSString stringWithFormat:@"%@",request.responseString]];
        //test
        
        if (error) {
         //general connection error for now
           // self.pFanClubResultError = error;
            completedWithFanClubResult(enumFanClubResultConnectionFail);
            return; //out 
        }//e
        
        else {
            //conn ok, method called. Parse return packet
            NSError *e = nil;
            
            id response = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&e];
            
            //self.pFanClubResultError = e;
            
            //return dat
            if ([response isKindOfClass:[NSDictionary class]]) {
                id email = [response objectForKey:@"email"];
                
                if ([email isKindOfClass:[NSString class]]) {
                    //subscribed
                    completedWithFanClubResult(enumFanClubResultSignUpEmailListSuccess);
                    return;
                }//e check
                
            }//return dat
            
            //return dat not parsable - possible err
            completedWithFanClubResult(enumFanClubResultSignUpEmailListFail);
            
        }//conn ok, method called. Parse return packet
        
    }];//andCompletionHandler
    
    //(success) ? completedWithFanClubResult(enumFanClubResultSignUpEmailListSuccess) : completedWithFanClubResult(enumFanClubResultSignUpEmailListFail);
    
}
////////////////////////////////////////////////////
#pragma mark - private method implementation

////////////////////////////////////////////////////
#pragma mark - delegate callbacks
@end
