//
//  FanClub.h
//
//  Created by Shin Tan on 1/8/14.
//  Copyright (c) 2014 PimpFit.com. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 * Desc:
 */

/**
 - TODO:
 */
////////////////////////////////////////////////
#pragma mark - public enum
enum {
    enumFanClubResultConnectionFail = 1,
    enumFanClubResultSignUpEmailListFail = 2,
    enumFanClubResultSignUpEmailListSuccess = 3
}; typedef int FanClubResult;

////////////////////////////////////////////////////
#pragma mark - public const

////////////////////////////////////////////////////
#pragma mark - public getter

/////////////////////////////////////////////////
#pragma mark - protocol delegate

/////////////////////////////////////////////////
#pragma mark - interface
@interface FanClub : NSObject

////////////////////////////////////////////////
#pragma mark - delegate property

////////////////////////////////////////////////////
#pragma mark - class property

//in
/**
 * main title
 */
@property(copy,nonatomic) NSString *pTitle;


/**
 * main msg
 */
@property(copy,nonatomic) NSString *pMsg;

/**
 * join command text
 */
@property(copy,nonatomic) NSString *pJoinClubCommandText;

/**
 * result success command text
 */
@property(copy,nonatomic) NSString *pResultSuccessCommandText;

/**
 * result fail command text
 */
@property(copy,nonatomic) NSString *pResultFailCommandText;


/**
 * result display text 1
 */
@property(copy,nonatomic) NSString *pResultFailText;

/**
 * result display text 2a
 */
@property(copy,nonatomic) NSString *pResultSuccessText1;

/**
 * result display text 2b
 */
@property(copy,nonatomic) NSString *pResultSuccessText2;

/**
 * fail image url
 */
@property(copy,nonatomic) NSString *pResultFailImageNamed;

/**
 * success image url
 */
@property(copy,nonatomic) NSString *pResultSuccessImageNamed;

/**
 * chimpkit api key
 */
@property(copy,nonatomic)NSString *pChimpKitApiKey;


/**
 * chimpkit list id from newly created mailchimp web mail list id
 */
@property(copy,nonatomic)NSString *pChimpKitListId;


/**
 cmd forecolor
 */
//@property(copy,nonatomic) UIColor *pCmdForeColor;

//out
@property(assign,nonatomic) FanClubResult pFanClubResult;
//@property(copy,nonatomic) NSError *pFanClubResultError;

////////////////////////////////////////////////////
#pragma mark - property of type enum:

////////////////////////////////////////////////////
#pragma mark - public const

////////////////////////////////////////////////////
#pragma mark - public getters

///////////////////////////////////////////////////
#pragma mark - public setters

////////////////////////////////////////////////////
#pragma mark - public static getters

////////////////////////////////////////////////////
#pragma mark - public static setters

/////////////////////////////////////////////////////////
#pragma mark - constructors

+(id) fanClubWithTitle:(NSString*)theTitle withMsg:(NSString*)theMsg withJoinClubCommandText:(NSString*)theJoinClubCommandText withResultSuccessCommandText:(NSString*)theResultSuccessCommandText withResultFailCommandText:(NSString*)theResultFailCommandText withResultFailText:(NSString*)theResultFailText withResultSuccessText1:(NSString*)theResultSuccessText1 withResultSuccessText2:(NSString*)theResultSuccessText2 withResultFailImageNamed:(NSString*)theResultFailImageNamed withResultSuccessImageNamed:(NSString*)theResultSuccessImageNamed withChimpKitApiKey:(NSString*)theApiKey withChimpKitListId:(NSString*)theListId ;
/////////////////////////////////////////////////////////
#pragma mark - class/static method declarations

/////////////////////////////////////////////////////////
#pragma mark - public method declarations

/**
 * email list signup.
 * Caller: handle fanClubResultCompletion block.
 * currently supporting ck API 3.0
 */
-(void) setSignUpForEmailListWithEmail:(NSString*)theEmail withFirstName:(NSString*)theFirstName withLastName:(NSString*)theLastName withFanClubResultCompletion:(void(^)(FanClubResult fanClubResultCompletion))completedWithFanClubResult;

@end
